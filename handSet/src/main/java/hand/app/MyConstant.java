package hand.app;

import hand.model.AccessInfo;
import hand.model.User_Info;
import hand.util.MyUtil;

public class MyConstant {

	public static final int SUCCESS_CODE = 1000;
	public static String testLog = "testLog";
	
	public static final String XFAppId = "58d0d4ef";

	/** 客服电话 */
	public static final String CONTACT_TELEPHONE = "4009196899";

	public static final String app_key = "62cd48dfb3945aa86293aac433733168";
	public static final String appSecret = "fc9dd303c24760ba321c341d29b89c68";
	public static String signature;

	public static AccessInfo accessInfo;
	public static User_Info userInfo;

	public static void initAccessInfo() {
		accessInfo = new AccessInfo();
		accessInfo.setApp_key(app_key);
	    accessInfo.setSignature(MyUtil.stringToMD5(MyConstant.appSecret));
	}
	
	public static AccessInfo getAccessInfo2() {
		AccessInfo accessInfo2 = new AccessInfo();
		accessInfo2.setApp_key(app_key);
		accessInfo2.setSignature(MyUtil.stringToMD5(MyConstant.appSecret));
		return accessInfo2;
	}

}
