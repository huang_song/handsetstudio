package hand.app;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;

import com.alibaba.fastjson.JSONObject;

import hand.ac.R;
import hand.http.HttpListener;
import hand.http.HttpUtil;
import hand.http.HttpUrl;
import hand.model.AppVersion;
import hand.util.MyUtil;
import tt.toolkit.SPUtil;
import tt.toolkit.ToastUtil;
import tt.toolkit.view.FlickerProgressBar;
import tt.toolkit.view.SimpleDialog;
import tt.toolkit.view.SimpleDialog.OnPositiveClickListener;
import tt.toolkit.JsonUtil;

public class UpdateManager {

	private String apkUrl; // 返回的安装包url
	private String savePath = "/sdcard/updatedemo/"; // 下载包安装路径
	private String saveFileName;
	private SimpleDialog noticeDialog;
	private Dialog downloadDialog;
	private FlickerProgressBar flickerProgressBar; // 进度条与通知ui刷新的handler和msg常量
	private int progress;
	private boolean isStop = false;
	private final int downing = 1;
	private final int downed = 2;
	private Context ctx;

	public UpdateManager(Context context) {
		this.ctx = context;
	}

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case downing:
				flickerProgressBar.setProgress(progress);
				break;
			case downed: // 安装APK
				flickerProgressBar.setFinish();
				downloadDialog.dismiss();
				File apkfile = new File(saveFileName);
				SPUtil.putInt(ctx, "isFirst", 0); // 标记是第一次进入APP
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.parse("file://" + apkfile.toString()), "application/vnd.android.package-archive");
				// 没有这句话,APP更新安装后不显示完成更新页面,不提示“完成，打开”，导致程序没启动！
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				ctx.startActivity(intent);
				break;
			}
		}
	};

	// 外部接口让主Activity调用
	public void checkUpdateInfo(final boolean showProcess) {
		
		HashMap map = new HashMap<String, Object>();
		JSONObject json = new JSONObject();
		json.put("platform", "android");
		json.put("type", "HandSet");
		map.put("data", json);
		
		HttpUtil http = new HttpUtil(HttpUrl.check_app_version, map, "检查版本");
		http.setOnHttpListener(new HttpListener() {
			@Override
			public void onHttpSuccess(int requestCode, Object obj) {
				String result = (String) obj;
				if (TextUtils.isEmpty(result)) {
					return;
				}
				AppVersion versionInfo = JsonUtil.json2Bean(result, AppVersion.class);
				if (showProcess && versionInfo == null) {
					ToastUtil.showToast(ctx, "当前已是最新版本");
					return;
				}
				apkUrl = versionInfo.getUrl();
				int newVersionCode = versionInfo.getVersionCode();
				int versionCode = MyUtil.getMyAppVersion(ctx).getVersionCode();
				if (newVersionCode > versionCode) {
					saveFileName = savePath + "HandSet" + newVersionCode + ".apk";
					showNoticeDialog(versionInfo.getForceFlg());
				} else {
					if (showProcess) {
						ToastUtil.showToast(ctx, "当前已是最新版本");
					}
				}
			}
				
			@Override
			public void onHttpFail(int requestCode, String errorMsg) { }
		});
		
		http.doPost(1, showProcess);
	}

	private void showNoticeDialog(final boolean forceUpdate) {
		if (noticeDialog == null) {
			noticeDialog = SimpleDialog.getInstance();
		}
		noticeDialog.setPositiveListener(new OnPositiveClickListener() {
			@Override
			public void onPositiveClick() {
				showDownloadDialog();
			}
		});
		if (forceUpdate) {
			noticeDialog.showSimpleDialog(ctx, "发现新版本哦，亲快下载吧~", false);
		} else {
			noticeDialog.showChooseDialog(ctx, "发现新版本哦，亲快下载吧~", "以后再说", "下载", false);
		}
	}

	private void showDownloadDialog() {
		downloadDialog = new Builder(ctx).create();
		downloadDialog.setCanceledOnTouchOutside(false);
		downloadDialog.setCancelable(false);
		downloadDialog.show();
		final LayoutInflater inflater = LayoutInflater.from(ctx);
		View downloadView = inflater.inflate(R.layout.dialog_update_progress, null);
		downloadDialog.setContentView(downloadView);
		flickerProgressBar = (FlickerProgressBar) downloadView.findViewById(R.id.progress);
		new Thread(downLoadRunnable).start(); // 下载APK
	}

	private Runnable downLoadRunnable = new Runnable() {
		@Override
		public void run() {
			try {
				URL url = new URL(apkUrl);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.connect();
				int length = conn.getContentLength();
				InputStream is = conn.getInputStream();
				File file = new File(savePath);
				if (!file.exists()) {
					file.mkdir();
				}
				String apkFile = saveFileName;
				File ApkFile = new File(apkFile);
				FileOutputStream fos = new FileOutputStream(ApkFile);

				int count = 0;
				byte buf[] = new byte[1024];
				do {
					int numread = is.read(buf);
					count += numread;
					progress = (int) (((float) count / length) * 100);
					mHandler.sendEmptyMessage(downing); // 更新进度
					if (numread <= 0) {
						mHandler.sendEmptyMessage(downed); // 下载完成通知安装
						break;
					}
					fos.write(buf, 0, numread);
				} while (!isStop); // 点击取消就停止下载.

				fos.close();
				is.close();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	};

}
