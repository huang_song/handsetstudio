package hand.app;

import com.iflytek.cloud.Setting;
import com.iflytek.cloud.SpeechUtility;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

public class KApplication extends Application {

	public static Activity currentActivity; 
	public static Context context;

	@SuppressLint("NewApi") @Override
	public void onCreate() {
		super.onCreate();
		context = this;
		SpeechUtility.createUtility(context, "appid=" + MyConstant.XFAppId);
		// 以下语句用于设置日志开关（默认开启），设置成false时关闭语音云SDK日志打印
		Setting.setShowLog(false);
		
	     this.registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {

			@Override
			public void onActivityCreated(Activity activity, Bundle savedInstanceState) { }

			@Override
			public void onActivityStarted(Activity activity) {
				 currentActivity = activity;  
			}

			@Override
			public void onActivityResumed(Activity activity) { }
			@Override
			public void onActivityPaused(Activity activity) { }
			@Override
			public void onActivityStopped(Activity activity) { }
			@Override
			public void onActivitySaveInstanceState(Activity activity, Bundle outState) { }
			@Override
			public void onActivityDestroyed(Activity activity) { }
	        });
	}

}