package hand.adapter;

import hand.ac.R;
import hand.model.ProductInfo;
import tt.toolkit.ViewHolder;
import java.util.List;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;

public class Appoint_Product_Dialog_Adapter extends BaseAdapter {

	private List<ProductInfo> productList;
	private int chooseItem = -1;

	public Appoint_Product_Dialog_Adapter(List<ProductInfo> reportList) {
		this.productList = reportList;
	}
	
	@Override
	public int getCount() {
		return productList != null ? productList.size() : 0;
	}
	@Override
	public Object getItem(int arg0) {
		return productList.get(arg0);
	}
	@Override
	public long getItemId(int arg0) {
		return arg0;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.one_radio_button_product, parent, false);
		}
		RadioButton radio_button = ViewHolder.get(convertView, R.id.radio_button);
		
		final ProductInfo productInfo = productList.get(position);
		radio_button.setText(productInfo.getName());
		if (position == chooseItem) {
			radio_button.setChecked(true);
		} else {
			radio_button.setChecked(false);
		}
		return convertView;
	}
	
	public void setChooseItem(int chooseItem) {
		this.chooseItem = chooseItem;
		notifyDataSetChanged();
	}
	
}
