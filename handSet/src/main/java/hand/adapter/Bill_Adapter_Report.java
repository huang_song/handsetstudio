package hand.adapter;

import hand.ac.R;
import hand.model.BillInfo;
import tt.toolkit.ViewHolder;
import java.util.List;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class Bill_Adapter_Report extends BaseAdapter {

	private List<BillInfo> billList;
	private Context ctx;

	public Bill_Adapter_Report(Context ctx) {
		this.ctx = ctx;
	}

	public Bill_Adapter_Report(Context ctx, List<BillInfo> billList) {
		this.billList = billList;
		this.ctx = ctx;
	}
	
	@Override
	public int getCount() {
		return billList != null ? billList.size() : 0;
	}
	@Override
	public Object getItem(int arg0) {
		return billList.get(arg0);
	}
	@Override
	public long getItemId(int arg0) {
		return arg0;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.bill_adapter_report, parent, false);
		}
		
		TextView txt_lading_num = ViewHolder.get(convertView, R.id.txt_lading_num);
		TextView txt_org_name = ViewHolder.get(convertView, R.id.txt_org_name);
		TextView txt_product_name = ViewHolder.get(convertView, R.id.txt_product_name);
		TextView txt_entrust_quantity = ViewHolder.get(convertView, R.id.txt_entrust_quantity);
		TextView txt_vehicle_no = ViewHolder.get(convertView, R.id.txt_vehicle_no);
		
		TextView txt_driver_name = ViewHolder.get(convertView, R.id.txt_driver_name);
		TextView txt_driver_phone = ViewHolder.get(convertView, R.id.txt_driver_phone);
		TextView txt_delivery_date = ViewHolder.get(convertView, R.id.txt_delivery_date);
		TextView txt_status_name = ViewHolder.get(convertView, R.id.txt_status_name);
		
		final BillInfo billInfo = billList.get(position);
		txt_lading_num.setText("提单编号：" + billInfo.getBillNo());
		txt_org_name.setText("采购方：" + billInfo.getDealerBusinessOrgName());
		txt_product_name.setText("产品名称：" + billInfo.getProductName());
		txt_entrust_quantity.setText("委托数量：" + billInfo.getAmount() + billInfo.getUnitName());
		txt_vehicle_no.setText("车牌号：" + billInfo.getVehicleNo());
		
		txt_driver_name.setText("司机姓名：" + billInfo.getDriverName());
		txt_driver_phone.setText(billInfo.getDriverPhone());
		txt_delivery_date.setText("提货日期：" + billInfo.getDeliveryAt());
		txt_status_name.setText(billInfo.getStatusName());
		
		return convertView;
	}
	
	public void setDatas(List<BillInfo> billList) {
		this.billList = billList;
		notifyDataSetChanged();
	}


}
