package hand.adapter;

import hand.ac.R;
import hand.http.HandSetHttp;
import hand.http.HttpListener;
import hand.model.BillInfo;
import hand.model.ProductInfo;
import tt.toolkit.JsonUtil;
import tt.toolkit.ToastUtil;
import tt.toolkit.ViewHolder;
import java.util.List;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class Bill_Adapter_Door extends BaseAdapter implements HttpListener {

	private List<BillInfo> billList;
	private Context ctx;
	private final int http_CanAppoint = 1;
	private final int http_appointProduct = 2;

	public Bill_Adapter_Door(Context ctx, List<BillInfo> billList) {
		this.billList = billList;
		this.ctx = ctx;
	}

	@Override
	public int getCount() {
		return billList != null ? billList.size() : 0;
	}

	@Override
	public Object getItem(int arg0) {
		return billList.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@SuppressLint("ResourceAsColor")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.bill_adapter_door, null);
		}
		
		TextView txt_serial_number = (TextView) convertView.findViewById(R.id.txt_serial_number);
		TextView txt_car_number = (TextView) convertView.findViewById(R.id.txt_car_number);
		TextView txt_product_name = (TextView) convertView.findViewById(R.id.txt_product_name);
		
		TextView txt_entrust_quantity = (TextView) convertView.findViewById(R.id.txt_entrust_quantity);
		TextView txt_valid_period = (TextView) convertView.findViewById(R.id.txt_valid_period);
		TextView txt_status_name = ViewHolder.get(convertView, R.id.txt_status_name);
		Button btn_appoint_product = (Button) convertView.findViewById(R.id.btn_appoint_product);
		
		final BillInfo billInfo = billList.get(position);
		txt_serial_number.setText("提单编号：" + billInfo.getBillNo());
		txt_product_name.setText("产品名称：" + billInfo.getProductName());
		txt_entrust_quantity.setText("委托数量：" + billInfo.getAmount() + billInfo.getUnitName());
		txt_car_number.setText("车牌号：" + billInfo.getVehicleNo());
		txt_valid_period.setText("单据有效期：" + billInfo.getDeliveryAt());
		txt_status_name.setText(billInfo.getStatusName());
		
		if (billInfo.isVirtualProduct()) {
			btn_appoint_product.setVisibility(View.VISIBLE);
			btn_appoint_product.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					HandSetHttp.getAppointProducts(new HttpListener() {
						@Override
						public void onHttpSuccess(int requestCode, Object obj) {
							String result = (String)obj;
							List<ProductInfo> productList = JsonUtil.json2BeanList(result, ProductInfo.class);
							showDialog(billInfo, productList);
						}
						
						@Override
						public void onHttpFail(int requestCode, String errorMsg) {
							ToastUtil.showToast(ctx, errorMsg, true);
						}
					}, billInfo.getProductId(), http_CanAppoint);
				}
			});
		} else {
			btn_appoint_product.setVisibility(View.GONE);
		}
		
		return convertView;
	}

	public void setDatas(List<BillInfo> billList) {
		this.billList = billList;
		notifyDataSetChanged();
	}
	
	ProductInfo productInfo;
	private void showDialog(final BillInfo billInfo, final List<ProductInfo> productList) {
		LinearLayout lay_dialog = (LinearLayout) LayoutInflater.from(ctx).inflate(R.layout.dialog_appoint_product, null);
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		final AlertDialog dialog = builder.create();
		dialog.setCancelable(true);
		dialog.show();
		dialog.setContentView(lay_dialog);
		
		final Appoint_Product_Dialog_Adapter adapter = new Appoint_Product_Dialog_Adapter(productList);
		ListView lv_product = (ListView) lay_dialog.findViewById(R.id.lv_product);
		lv_product.setAdapter(adapter);
		TextView txt_cancel = (TextView) lay_dialog.findViewById(R.id.txt_cancel);
		TextView txt_sure = (TextView) lay_dialog.findViewById(R.id.txt_sure);
		
		productInfo = null;
		lv_product.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				productInfo = productList.get(position);
				adapter.setChooseItem(position);
			}
		});
		
		txt_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		
		txt_sure.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (null == productInfo) {
					ToastUtil.showToast(ctx, "请先选择指定产品");
				} else {
					HandSetHttp.appointProduct(Bill_Adapter_Door.this, billInfo, productInfo.getId(), http_appointProduct);
					dialog.dismiss();
				}
			}
		});
	}


	@Override
	public void onHttpSuccess(int requestCode, Object obj) {
		switch (requestCode) {
		case http_appointProduct:
			ToastUtil.showToast(ctx, "指定产品成功");
			String result = (String)obj;
			BillInfo billInfo = JsonUtil.json2Bean(result, BillInfo.class);
			for (int i = 0; i < billList.size(); i++) {
				if (billList.get(i).getId() == billInfo.getId()) {
					billList.set(i, billInfo);
					notifyDataSetChanged();
					break;
				}
			}
			break;
		}
	}

	@Override
	public void onHttpFail(int requestCode, String errorMsg) {
		ToastUtil.showToast(ctx, errorMsg, true);
	}

}
