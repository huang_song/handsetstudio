package hand.adapter;

import hand.ac.R;
import hand.http.HandSetHttp;
import hand.http.HttpListener;
import hand.model.BillInfo;
import tt.toolkit.ToastUtil;
import tt.toolkit.view.SimpleDialog;
import tt.toolkit.view.SimpleDialog.OnPositiveClickListener;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;

public class Bill_Adapter_Car extends BaseAdapter {

	private List<BillInfo> billList;
//	private List<WareHouse> houseList;
//	private WareHouse selectHouse;
	private Context ctx;

	public Bill_Adapter_Car(Context ctx) {
		this.ctx = ctx;
	}

	public Bill_Adapter_Car(Context ctx, List<BillInfo> billList) {
		this.billList = billList;
//		this.houseList = houseList;
		this.ctx = ctx;
	}
	
	@Override
	public int getCount() {
		return billList != null ? billList.size() : 0;
	}
	@Override
	public Object getItem(int arg0) {
		return billList.get(arg0);
	}
	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	class ViewHolder {
		TextView txt_serial_number;    // 提单编号
		TextView txt_dealer_name;      // 采购方
		TextView txt_product_name;	   // 产品名称
		TextView txt_entrust_quantity; // 委托数量
		TextView txt_car_number;	   // 车号
		
		EditText edit_package_num;  // 件数
//		Spinner spinner;  // 库位
//		String location;  // spinner中选中库位
		TextView txt_valid_period; // 单据有效期
		
		LinearLayout lay_modify_info;
		TextView txt_modify_info;  // 返库信息
		TextView txt_modify;
		EditText edit_modify_num;
		Button btn_confirm;
	}

	@SuppressLint("ResourceAsColor") @Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			convertView = inflater.inflate(R.layout.bill_adapter_car, null);
			holder = new ViewHolder();
			holder.txt_serial_number = (TextView) convertView.findViewById(R.id.txt_serial_number);
			holder.txt_dealer_name = (TextView) convertView.findViewById(R.id.txt_dealer_name);
			holder.txt_product_name = (TextView) convertView.findViewById(R.id.txt_product_name);
			holder.txt_entrust_quantity = (TextView) convertView.findViewById(R.id.txt_entrust_quantity);
			holder.txt_car_number = (TextView) convertView.findViewById(R.id.txt_car_number);
			holder.txt_valid_period = (TextView) convertView.findViewById(R.id.txt_valid_period);
			
			holder.edit_package_num = (EditText) convertView.findViewById(R.id.edit_package_num);
//			holder.spinner = (Spinner) convertView.findViewById(R.id.spinner_location);
			
			holder.lay_modify_info = (LinearLayout) convertView.findViewById(R.id.lay_modify_info);
			holder.txt_modify_info = (TextView) convertView.findViewById(R.id.txt_modify_info);
			holder.txt_modify = (TextView) convertView.findViewById(R.id.txt_modify);
			holder.edit_modify_num = (EditText) convertView.findViewById(R.id.edit_modify_num);
			holder.btn_confirm = (Button) convertView.findViewById(R.id.btn_confirm);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		final BillInfo billInfo = billList.get(position);
		holder.txt_serial_number.setText("提单编号：" + billInfo.getBillNo());
		holder.txt_dealer_name.setText("采购方：" + billInfo.getDealerBusinessOrgName());
		holder.txt_product_name.setText("产品名称：" + billInfo.getProductName());
		holder.txt_entrust_quantity.setText("委托数量：" + billInfo.getAmount() + billInfo.getUnitName());
		holder.txt_car_number.setText("车牌号：" + billInfo.getVehicleNo());
		holder.txt_valid_period.setText("单据有效期：" + billInfo.getDeliveryAt());
		
//		final ArrayList<String> strList = new ArrayList<String>();
//		for(WareHouse house : houseList) {
//			strList.add(house.getCode());
//		}
//		
//		ArrayAdapter<String> house_Adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_item, strList);
//		house_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); //没有也行，但点击spinner出来的列表很小
//		holder.spinner.setAdapter(house_Adapter);
//		
//		spinner的点击监听
//		holder.spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
//			@Override
//			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//				selectHouse = houseList.get(position);
//			}
//			@Override
//			public void onNothingSelected(AdapterView<?> parent) { }
//		});
		
		holder.btn_confirm.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				/*String numStr;
				BigDecimal package_num = null;
				if ("StoreAdd".equals( billInfo.getStoreOperateStatus())) {
					numStr = holder.edit_modify_num.getText().toString().trim();
					if (TextUtils.isEmpty(numStr)) {
						ToastUtil.showToast(ctx, "请输入件数");
						return;
					} else if (numStr.startsWith("0")) {
						ToastUtil.showToast(ctx, "请输入有效件数");
						return;
					}
					package_num = new BigDecimal(numStr);
					package_num = billInfo.getStorePackagesNumber().add(package_num);
					showAlertDialog(billInfo, package_num, "确认补货数量：" + numStr);
				} else if ("StoreSubduct".equals(billInfo.getStoreOperateStatus())){
					numStr = holder.edit_modify_num.getText().toString().trim();
					if (TextUtils.isEmpty(numStr)) {
						ToastUtil.showToast(ctx, "请输入件数");
						return;
					} else if (numStr.startsWith("0")) {
						ToastUtil.showToast(ctx, "请输入有效件数");
						return;
					}
					package_num = new BigDecimal(numStr);
					package_num = billInfo.getStorePackagesNumber().subtract(package_num);
					
					BigDecimal num = new BigDecimal("0");   // 用做比较的值
					if (package_num.compareTo(num) == -1) { // 小于时，返回-1
						ToastUtil.showToast(ctx, "卸货件数不能大于装货件数", true);
						return;
					}
					showAlertDialog(billInfo, package_num, "确认卸货数量：" + numStr);
				} else {
					numStr = holder.edit_package_num.getText().toString().trim();
					if (TextUtils.isEmpty(numStr)) {
						ToastUtil.showToast(ctx, "请输入件数");
						return;
					} else if (numStr.startsWith("0")) {
						//
						ToastUtil.showToast(ctx, "请输入有效件数");
						return;
					}
					package_num = new BigDecimal(numStr);
					showAlertDialog(billInfo, package_num, "确认装货数量：" + numStr);
				}*/
				showAlertDialog(billInfo, new BigDecimal(0), "确认出库 ？");
			}
		});
		
		holder.lay_modify_info.setVisibility(View.GONE);
		
		holder.edit_package_num.getText().clear(); ///////////原单1-补货-原单2（防止出现原单1补货里的packagesNumber）
		holder.edit_package_num.setEnabled(true);
		holder.edit_package_num.setBackgroundResource(R.drawable.shape_solid_white_stroke_gray2_radius5);
		if ("StoreAdd".equals(billInfo.getStoreOperateStatus())) {
			holder.edit_package_num.setText(billInfo.getStorePackagesNumber() + "");
			holder.lay_modify_info.setVisibility(View.VISIBLE);
			holder.edit_package_num.setBackgroundResource(R.color.line_gray2);
			holder.edit_package_num.setEnabled(false);
//			for (int i = 0; i < houseList.size(); i++) {
//				if (houseList.get(i).getId() == billInfo.getWarehouseId()) {
//					holder.spinner.setSelection(i);
//					break;
//				}
//			}
			holder.txt_modify_info.setText("补货数量：" + billInfo.getStoreAddAmount() + "    单位：" + billInfo.getUnitName());
			holder.txt_modify.setText("补货件数：+ ");
		} else if ("StoreSubduct".equals(billInfo.getStoreOperateStatus())) {
			holder.edit_package_num.setText(billInfo.getStorePackagesNumber() + "");
			holder.lay_modify_info.setVisibility(View.VISIBLE);
			holder.edit_package_num.setBackgroundResource(R.color.line_gray2);
			holder.edit_package_num.setEnabled(false);
//			for (int i = 0; i < houseList.size(); i++) {
//				if (houseList.get(i).getId() == billInfo.getWarehouseId()) {
//					holder.spinner.setSelection(i);
//					break;
//				}
//			}
			holder.txt_modify_info.setText("卸货数量：" + billInfo.getStoreSubductAmount() + "    单位：" + billInfo.getUnitName());
			holder.txt_modify.setText("卸货件数：- ");
		}
		
		return convertView;
	}
	
	public void setDatas(List<BillInfo> billList) {
		this.billList = billList;
//		this.houseList = houseList;
//		selectHouse  = houseList.get(0);
		notifyDataSetChanged();
	}

	// 确认装车对话框
	private void showAlertDialog(final BillInfo billInfo, final BigDecimal package_num, String dialogTitle) {
		SimpleDialog dialog = SimpleDialog.getInstance();
		final String StoreType = billInfo.getStoreOperateStatus();
		dialog.setPositiveListener(new OnPositiveClickListener() {
			@Override
			public void onPositiveClick() {
				HashMap map = new HashMap<String, Object>();
				JSONObject json = new JSONObject();
				json.put("id", billInfo.getId());
				json.put("version", billInfo.getVersion());
				json.put("storePackagesNumber", package_num);
//				json.put( "warehouse", new JSONObject().put("id", house.getId()) );
				map.put("data", json);
				if ("StoreAdd".equals(StoreType) || "StoreSubduct".equals(StoreType)) {
					HandSetHttp.LoadingEdit(new HttpListener() {
						@Override
						public void onHttpSuccess(int requestCode, Object obj) {
							if ("StoreSubduct".equals(StoreType)) {
								ToastUtil.showToast(ctx, "卸货成功");
							} else {
								ToastUtil.showToast(ctx, "补货成功");
							}
							billList.remove(billInfo);
							notifyDataSetChanged();
						}
						
						@Override
						public void onHttpFail(int requestCode, String errorMsg) {
							ToastUtil.showToast(ctx, errorMsg, true);
						}
					}, map, 1);
				} else {
					HandSetHttp.Loading(new HttpListener() {
						@Override
						public void onHttpSuccess(int requestCode, Object obj) {
							ToastUtil.showToast(ctx, "装货成功");
							billList.remove(billInfo);
							notifyDataSetChanged();
						}
						
						@Override
						public void onHttpFail(int requestCode, String errorMsg) {
							ToastUtil.showToast(ctx, errorMsg, true);
						}
					}, map, 2);
				}
			}
		});
		dialog.showChooseDialog(ctx, dialogTitle, "取消", "确定", false);
	}

}
