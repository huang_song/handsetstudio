package hand.http;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class MyX509TrustManager {

	public static HttpURLConnection initHttps(String str) throws IOException {
		URL url = new URL(str);
		HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
		if (str.startsWith("https")) {
			HttpsURLConnection httpsCon = (HttpsURLConnection) httpCon;
			trustAllHosts(httpsCon);
			httpsCon.setHostnameVerifier(DO_NOT_VERIFY);
		}
		httpCon.setRequestMethod("POST");
		httpCon.setConnectTimeout(20000);	// 连接超时
		httpCon.setReadTimeout(25000);		// 读取超时（服务器响应比较慢，增大时间）
		// 发送POST请求必须设置如下两行
		httpCon.setDoOutput(true);
		httpCon.setDoInput(true);
		// Post 请求不能使用缓存
		httpCon.setUseCaches(false);
		return httpCon;
	}

	/** 信任所有 */
	private static void trustAllHosts(HttpsURLConnection httpsCon) {
		/** 覆盖java默认的证书验证 */
		TrustManager[] tm = new TrustManager[] { new TrustAllManager() };
		try {
			SSLContext sslCtx = SSLContext.getInstance("TLS");
//			SSLContext sslCtx = SSLContext.getInstance("SSL"); // 这个也行
			sslCtx.init(null, tm, new SecureRandom());
			SSLSocketFactory factory = sslCtx.getSocketFactory();
			httpsCon.setSSLSocketFactory(factory);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static class TrustAllManager implements X509TrustManager {
		@Override
		public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException { }
		@Override
		public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException { }
		@Override
		public X509Certificate[] getAcceptedIssuers() {
			return null;
//			return new X509Certificate[] {}; // 这个也行
		}
	}

	/** 设置不验证主机 */
	private static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	};

}