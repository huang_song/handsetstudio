package hand.http;

import hand.app.KApplication;
import hand.app.MyConstant;
import hand.model.AccessInfo;
import hand.util.MyUtil;
import tt.toolkit.JsonUtil;
import tt.toolkit.TTUtil;
import tt.toolkit.LogUtil;
import tt.toolkit.view.RoundProcessDialog;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.conn.ConnectTimeoutException;
import android.os.Handler;
import android.os.Message;

public class HttpUtil {

	private HttpURLConnection client;
	private String urlStr;
	public Map<String, Object> params = new HashMap<String, Object>();
	private String interfaceName;

	private HttpListener listener;
	private int requestCode;
	private String tag_in = "HSHttp_in";   // 请求日志
	private String tag_out = "HSHttp_out"; // 返回日志
	private static final int http_success = 1;
	private static final int http_fail = 2;


	public void setOnHttpListener(HttpListener httpListener) {
		this.listener = httpListener;
	}
	
	public HttpUtil(String url, Map<String, Object> params, String interfaceName) {
		this.urlStr = url;
		this.params = params;
		this.interfaceName = interfaceName;
	}
	
	//	创建请求参数
	private String createParam() {
		AccessInfo accessInfo;
		if (urlStr.equals(HttpUrl.check_app_version)) {
			accessInfo = MyConstant.getAccessInfo2();
		} else {
			accessInfo = MyConstant.accessInfo;
		}
		if (params == null) {
			params = new HashMap<String, Object>();
		}
		params.put("accessInfo", accessInfo);
		return JsonUtil.map2Json(params);
	}

	/** post请求  */
	public void doPost(int requestCode, boolean showProcess) {
		this.requestCode = requestCode;
		final RoundProcessDialog processDialog = new RoundProcessDialog();
		if (showProcess) {
			processDialog.showRoundProcessDialog(KApplication.currentActivity, null);
		}
		new Thread( ()->requestStart(processDialog, createParam()) ).start();
	}

	private void requestStart(RoundProcessDialog processDialog, String param) {
		Message msg = Message.obtain();
		if (!TTUtil.isNetConnected(KApplication.context)) {
			processDialog.dismissRoundProcessDialog();
			msg.what = http_fail;
			msg.obj = "网络异常";
			handler.sendMessage(msg);
			return;
		}

		try {
			LogUtil.i(tag_in, interfaceName + ": "+ urlStr + "\n入参-->" + "\n" + param);
			client = MyX509TrustManager.initHttps(urlStr);
			client.setRequestProperty("Content-Type", "application/json"); // 发送json数据
			// 连接,也可以不用明文connect，使用httpConn.getOutputStream()会自动connect
			// httpConn.connect();
			OutputStream outStrm = client.getOutputStream();
			byte[] bs = param.getBytes();
			outStrm.write(bs);
			outStrm.flush();
			outStrm.close();

			// 获得响应状态
			int resultCode = client.getResponseCode();
			InputStream inStrm;
			String result;
			if (resultCode == HttpURLConnection.HTTP_OK) {
				inStrm = client.getInputStream();
				result = MyUtil.stream2String(inStrm);
				msg.what = http_success;
				msg.obj = result;
			} else {
//				https://blog.csdn.net/q2232/article/details/48136973  参考知识点
				inStrm = client.getErrorStream();
				result = MyUtil.stream2String(inStrm);
				String errorStr = "服务器异常";
				errorStr = JsonUtil.getString(result, "errors");
				msg.what = http_fail;
				msg.obj = errorStr;
			}
			LogUtil.e(tag_out, interfaceName + ": 返回码-->" + resultCode + "\n出参-->" + "\n" + result);
		} catch (ConnectTimeoutException e){
			msg.what = http_fail;
			msg.obj = "网络请求超时";
			e.printStackTrace();
			return;
		} catch (Exception e) {
//			msg.what = http_fail;
//			msg.obj = "未知异常：" + e;
			LogUtil.e(tag_out, interfaceName + ": 未知异常-->" + "\n" + e);
			e.printStackTrace();
		} finally {
			processDialog.dismissRoundProcessDialog();
			handler.sendMessage(msg);
		}
	}
	
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case http_success:
				if (listener != null) {
					listener.onHttpSuccess(requestCode, msg.obj.toString());
				}
				break;
			case http_fail:
				if (listener != null) {
					listener.onHttpFail(requestCode, msg.obj.toString());
				}
				break;
			}
		};
	};
	
}
