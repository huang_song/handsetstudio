package hand.http;

import com.alibaba.fastjson.JSONObject;

import hand.model.BillInfo;
import hand.model.TrainBill;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/** 用户信息请求接口 */
public class HandSetHttp {

	/** 获取提货单 */
	public static void getBill(HttpListener listener, String idCard, int requestCode) {
		HashMap map = new HashMap<String, String>();
		map.put("idCard", idCard);
		HttpUtil http = new HttpUtil(HttpUrl.json_get_bill, map, "获取提货单");
		http.setOnHttpListener(listener);
		http.doPost(requestCode, true);
	}
	
	/** 获取库位 */
//	public static void getWarehouse(HttpListener listener, int requestCode) {
//		HashMap map = new HashMap<String, String>();
//		map.put("name", "库位");
//		HSHttp http = new HSHttp(HttpUrl.json_get_warehouse, map, "获取库位");
//		http.setOnHttpListener(listener);
//		http.doPost(requestCode, true);
//	}
	
	
	/** 装车接口 */
	public static void Loading(HttpListener listener, Map<String, Object> map, int requestCode) {
		HttpUtil http = new HttpUtil(HttpUrl.json_lading, map, "装车接口");
		http.setOnHttpListener(listener);
		http.doPost(requestCode, true);
	}
	
	
	/** 库房补卸货 */
	public static void LoadingEdit(HttpListener listener, Map<String, Object> map, int requestCode) {
		HttpUtil http = new HttpUtil(HttpUrl.json_lading_edit, map, "库房补卸货");
		http.setOnHttpListener(listener);
		http.doPost(requestCode, true);
	}
	
	
	/** 火车获取提货单 */
	public static void getTrainBill(HttpListener listener, String trainNo, int requestCode) {
		HashMap map = new HashMap<String, String>();
		map.put("data", trainNo);
		HttpUtil http = new HttpUtil(HttpUrl.json_get_trainbill, map, "获取火车提货单");
		http.setOnHttpListener(listener);
		http.doPost(requestCode, true);
	}

	
	/** 火车装车接口 */
	public static void trainLoading(HttpListener listener, TrainBill trainBill, BigDecimal package_num, int requestCode) {
		HashMap map = new HashMap<String, Object>();
		JSONObject json = new JSONObject();
		json.put("id", trainBill.getId());
		json.put("version", trainBill.getVersion());
		json.put("packagesNumber", package_num);
		map.put("data", json);
		
		HttpUtil http = new HttpUtil(HttpUrl.json_trainlading, map, "火车装车接口");
		http.setOnHttpListener(listener);
		http.doPost(requestCode, true);
	}
	
	
	/** 获取报表 */
	public static void getBill2(HttpListener listener, Map<String, Object> map, int requestCode, boolean showProcess) {
		HttpUtil http = new HttpUtil(HttpUrl.json_get_bill2, map, "获取报表");
		http.setOnHttpListener(listener);
		http.doPost(requestCode, showProcess);
	}
	
	
	/** 门卫-进出厂 */
	public static void getDoorBill(HttpListener listener, String idCard, int requestCode) {
		HashMap map = new HashMap<String, String>();
		map.put("data", idCard);
		HttpUtil http = new HttpUtil(HttpUrl.in_out_door, map, "门卫-进出厂");
		http.setOnHttpListener(listener);
		http.doPost(requestCode, true);
	}
	
	/** 获取可指定产品 */
	public static void getAppointProducts(HttpListener listener, String productId, int requestCode) {
		HashMap map = new HashMap<String, String>();
		map.put("data", productId);
		HttpUtil http = new HttpUtil(HttpUrl.can_appoint_products, map, "获取可指定产品");
		http.setOnHttpListener(listener);
		http.doPost(requestCode, true);
	}
	
	/** 指定产品 */
	public static void appointProduct(HttpListener listener, BillInfo billInfo, String productId, int requestCode) {
		HashMap map = new HashMap<String, Object>();
		JSONObject json = new JSONObject();
		JSONObject json2 = new JSONObject();
		json2.put("id", productId);
		json.put("id", billInfo.getId());
		json.put("version", billInfo.getVersion());
		json.put("product", json2);
		map.put("data", json);
		
		HttpUtil http = new HttpUtil(HttpUrl.appoint_product, map, "指定产品");
		http.setOnHttpListener(listener);
		http.doPost(requestCode, true);
	}
	
	
}
