package hand.http;

public class HttpUrl {
	
//	public final static String json_root = "https://www.zhiyunlanhai.com/es/"; // 线上
//	public final static String json_root = "http://114.215.44.90:8080/es/";    // 备机
//	public final static String json_root = "https://192.168.0.114:7443/es/";   // 线下1
//	public final static String json_root = "https://192.168.0.114:8443/es/";   // 线下2
//	public final static String json_root = "https://192.168.0.7:8443/es/";    // 周伟
	
	public final static String json_root = "http://192.168.0.110:8001/es/";
	
	
	/** 获取用户md5 */
	public final static String json_getMD5 = json_root + "userMD5";
	/** 登录 */
	public final static String json_login = json_root + "login";
	/** 退出登录 */
	public final static String json_login_out = json_root + "logout";
	/** 获取功能权限 */
	public final static String json_function = json_root + "main/getHandSetFuncTree";
	
	/** 获取重置密码验证码 */
	public final static String json_reset_pwd_code = json_root + "sendResetPWDCode";
	/** 重置密码 */
	public final static String json_reset_pwd = json_root + "resetPwd";
	
	/** 获取修改手机验证码 */
	public final static String json_phone_code = json_root + "changePhoneCode";
	/** 修改手机号 */
	public final static String json_update_phone = json_root + "updatePhone";
	
	/** 修改密码 */
	public final static String json_update_pwd = json_root + "updatePwd";
	public final static String check_app_version = json_root + "AppVersion/lastVersion";
	
	/** 提货单 */
	public final static String json_get_bill = json_root + "LadingBill/listLading";
	/** 库位 */
	public final static String json_get_warehouse = json_root + "Ref/data";
	/** 装车 */
	public final static String json_lading = json_root + "LadingBill/lading";
	/** 库房补卸货 */
	public final static String json_lading_edit = json_root + "LadingBill/storeHousEdit";
	
	
	/** 火车提货单 */
	public final static String json_get_trainbill = json_root + "LadingTrainApp/listLading";
	/** 火车装车 */
	public final static String json_trainlading = json_root + "LadingTrainApp/lading";
	
	/** 获取报表 */
	public final static String json_get_bill2 = json_root + "HandSet/LadingBillReport/list";
	
	
	/** 门卫-进出厂 */
	public final static String in_out_door = json_root + "LadingBill/handset/incomingOrOutgoing";
	/** 获取可指定产品 */
	public final static String can_appoint_products = json_root + "Product/getCanAppointProducts";
	/** 指定产品 */
	public final static String appoint_product = json_root + "LadingBill/handset/appointProduct";
	
}
