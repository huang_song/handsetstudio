package hand.http;

import java.util.HashMap;
import java.util.Map;

/** 用户信息请求接口 */
public class UserHttp {
	
	/** 获取用户md5 */
	public static void getUserMD5(HttpListener listener, int requestCode) {
		HttpUtil http = new HttpUtil(HttpUrl.json_getMD5, null, "获取用户md5");
		http.setOnHttpListener(listener);
		http.doPost(requestCode, true);
	}
	
	
	/** 登录请求 */
	public static void loginRquest(HttpListener listener, int requestCode) {
		Map map = new HashMap<String, Object>();
		map.put("clientType", "HandSet");
		HttpUtil http = new HttpUtil(HttpUrl.json_login, map, "登录请求");
		http.setOnHttpListener(listener);
		http.doPost(requestCode, true);
	}
	
	/** 获取功能权限 */
	public static void getFunction(HttpListener listener, int requestCode) {
		HttpUtil http = new HttpUtil(HttpUrl.json_function, null, "获取功能权限");
		http.setOnHttpListener(listener);
		http.doPost(requestCode, true);
	}
	
	
	/** 获取重置密码验证码 */
	public static void getResetPwdCode(HttpListener listener, int requestCode) {
		HttpUtil http = new HttpUtil(HttpUrl.json_reset_pwd_code, null, "获取重置密码验证码");
		http.setOnHttpListener(listener);
		http.doPost(requestCode, false);
	}
	
	
	/**   重置密码 */
	public static void resetPwd(HttpListener listener, Map<String, Object> map, int requestCode) {
		HttpUtil http = new HttpUtil(HttpUrl.json_reset_pwd, map, "重置密码");
		http.setOnHttpListener(listener);
		http.doPost(requestCode, true);
	}

	
	/** 获取修改手机号验证码 */
	public static void getPhoneCode(HttpListener listener, int requestCode) {
		HttpUtil http = new HttpUtil(HttpUrl.json_phone_code, null, "获取修改手机号验证码");
		http.setOnHttpListener(listener);
		http.doPost(requestCode, false);
	}
	
	
	/** 修改手机号 */
	public static void updatePhone(HttpListener listener, Map<String, Object> map, int requestCode) {
		HttpUtil http = new HttpUtil(HttpUrl.json_update_phone, map, "修改手机号");
		http.setOnHttpListener(listener);
		http.doPost(requestCode, true);
	}
	
	
	/** 修改密码 */
	public static void updatePwd(HttpListener listener, Map<String, Object> map, int requestCode) {
		HttpUtil http = new HttpUtil(HttpUrl.json_update_pwd, map, "修改密码");
		http.setOnHttpListener(listener);
		http.doPost(requestCode, true);
	}
	
	
	/** 退出登录 */
	public static void loginOut(HttpListener listener, int requestCode) {
		HttpUtil http = new HttpUtil(HttpUrl.json_login_out, null, "退出登录");
		http.setOnHttpListener(listener);
		http.doPost(requestCode, false);
	}
	

}
