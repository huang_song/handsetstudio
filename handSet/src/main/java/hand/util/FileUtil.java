package hand.util;

import java.io.File;
import java.io.FileInputStream;
import java.text.DecimalFormat;

public class FileUtil {
	
	/** 获取文件大小为B，KB，MB，GB的double值 */
	public enum SizeType {
		SIZETYPE_B, SIZETYPE_KB, SIZETYPE_MB, SIZETYPE_GB
	}

	/** 获取文件指定单位的大小 */
	public static double getFileOrFilesSize(String filePath, SizeType sizeType) {
		File file = new File(filePath);
		long fileSize = 0;
		try {
			if (file.isDirectory()) {
				fileSize = getFileSizes(file);
			} else {
				fileSize = getFileSize(file);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return FormetFileSize(fileSize, sizeType);
	}

	/**
	 * 调用此方法自动计算指定文件或指定文件夹的大小
	 * @return 计算好的带B、KB、MB、GB的字符串
	 */
	public static String getAutoFileOrFilesSize(String filePath) {
		File file = new File(filePath);
		long fileSize = 0;
		try {
			if (file.isDirectory()) {
				fileSize = getFileSizes(file);
			} else {
				fileSize = getFileSize(file);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return FormetFileSize(fileSize);
	}

	/** 获取指定文件大小 */
	public static long getFileSize(File file) throws Exception {
		long fileSize = 0;
		if (file.exists()) {
			FileInputStream fis = null;
			fis = new FileInputStream(file);
			fileSize = fis.available();
		}
		return fileSize;
	}

	/** 获取指定文件夹大小 */
	public static long getFileSizes(File file) throws Exception {
		long fileSize = 0;
		File flist[] = file.listFiles();
		for (int i = 0; i < flist.length; i++) {
			if (flist[i].isDirectory()) {
				fileSize = fileSize + getFileSizes(flist[i]);
			} else {
				fileSize = fileSize + getFileSize(flist[i]);
			}
		}
		return fileSize;
	}

	/** 转换文件大小 */
	public static String FormetFileSize(long fileSize) {
		DecimalFormat df = new DecimalFormat("#.00");
		String fileSizeString = "";
		String wrongSize = "0B";
		if (fileSize == 0) {
			return wrongSize;
		}
		if (fileSize < 1024) {
			fileSizeString = df.format((double) fileSize) + "B";
		} else if (fileSize < 1048576) {
			fileSizeString = df.format((double) fileSize / 1024) + "KB";
		} else if (fileSize < 1073741824) {
			fileSizeString = df.format((double) fileSize / 1048576) + "MB";
		} else {
			fileSizeString = df.format((double) fileSize / 1073741824) + "GB";
		}
		return fileSizeString;
	}

	/** 转换文件大小,指定转换的类型 */
	public static double FormetFileSize(long fileS, SizeType type) {
		DecimalFormat df = new DecimalFormat("#.00");
		double fileSize = 0;
		switch (type) {
		case SIZETYPE_B:
			fileSize = Double.valueOf(df.format((double) fileS));
			break;
		case SIZETYPE_KB:
			fileSize = Double.valueOf(df.format((double) fileS / 1024));
			break;
		case SIZETYPE_MB:
			fileSize = Double.valueOf(df.format((double) fileS / 1048576));
			break;
		case SIZETYPE_GB:
			fileSize = Double.valueOf(df.format((double) fileS / 1073741824));
			break;
		default:
			break;
		}
		return fileSize;
	}
	
	/** 删除文件夹里面所有的文件 */
	public static void deleteFile(File file) {
		if (file == null) {
			return;
		}
		if (!file.exists() || !file.canWrite()) {	// || file.getAbsolutePath().contains(DbHelper.dbn)
			return;
		}
		if (!file.isDirectory()) {
			file.delete();
		} else {
			File[] files = file.listFiles();
			for (int i = 0; i < files.length; i++) {
				deleteFile(files[i]);
			}
		}
	}
	
	
}
