package hand.util;

import hand.model.AppVersion;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Paint;
import android.inputmethodservice.ExtractEditText;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.TextView;

public class MyUtil {

	/** 获取APP版本信息(版本号,版本名称) */
	public static AppVersion getMyAppVersion(Context context) {
		PackageManager manager = context.getPackageManager();
		PackageInfo info = new PackageInfo();
		try {
			info = manager.getPackageInfo(context.getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		AppVersion version = new AppVersion();
		version.setVersionName(info.versionName);
		version.setVersionCode(info.versionCode);
		return version;
	}

	/** 获取APP渠道号 */
	public static String getChannel(Context context) {
		String channel = "";
		try {
			PackageManager manager = context.getPackageManager();
			String packageName = context.getPackageName();
			ApplicationInfo info = manager.getApplicationInfo(packageName, PackageManager.GET_META_DATA);
			Bundle bundle = info.metaData;
			Object value = bundle.get("BaiduMobAd_CHANNEL");
			channel = value.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return channel;
	}

	// 得到屏幕宽度
	public static int getScreenWidth(Activity activity) {
		DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int width = metrics.widthPixels;
		return width;
	}

	// 得到屏幕宽度
	public static int getScreenHeight(Activity activity) {
		DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int height = metrics.heightPixels;
		return height;
	}

	/** 将dip值动态转换成px值 */
	public static int dip2px(Context context, float dpValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dpValue * scale + 0.5f);
	}

	public static int dp2px(Context context, int dp) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
	}


	/** 获取4位数随机验证码 Random */
	public static String randomText() {
		Random random = new Random();
		Set<Integer> set = new HashSet<Integer>();
		while (set.size() < 4) {
			int randomInt = random.nextInt(10);
			set.add(randomInt);
		}
		StringBuffer sb = new StringBuffer();
		for (Integer i : set) {
			sb.append("" + i);
		}
		return sb.toString();
	}

	/** 
	 * 将字符串转成MD5值 
	 *  
	 * @param string 
	 * @return 
	 */  
	public static String stringToMD5(String string) {
	    byte[] hash;
	  
	    try {
	        hash = MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"));  
	    } catch (NoSuchAlgorithmException e) {
	        e.printStackTrace();
	        return null;
	    } catch (UnsupportedEncodingException e) {
	        e.printStackTrace();
	        return null;
	    }
	  
	    StringBuilder hex = new StringBuilder(hash.length * 2);
	    for (byte b : hash) {
	        if ((b & 0xFF) < 0x10)
	            hex.append("0");
	        hex.append(Integer.toHexString(b & 0xFF));
	    }
	  
	    return hex.toString().toUpperCase();
	}  


	/** 防止重复点击 */
	private static long lastClickTime;

	public synchronized static boolean isFastClick() {
		long time = System.currentTimeMillis();
		if (time - lastClickTime < 5000) {
			return true;
		}
		lastClickTime = time;
		return false;
	}

	/** 设置TextView下划线 */
	public static void setUnderLine(TextView tv) {
		tv.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
		tv.getPaint().setAntiAlias(true);
	}

	/** Activity跳转 */
	public static void switchToActivity(Context ctx, Class<?> cls) {
		Intent intent = new Intent(ctx, cls);
		ctx.startActivity(intent);
	}

	public static String stream2String(InputStream inStream) throws Exception {
		BufferedReader responseReader = new BufferedReader(new InputStreamReader(inStream, "UTF-8"));
		String readLine;
		StringBuffer sb = new StringBuffer();
		while ((readLine = responseReader.readLine()) != null) {
			sb.append(readLine).append("\n");
		}
		responseReader.close();
		String result = sb.toString();
		return result;
	}

}
