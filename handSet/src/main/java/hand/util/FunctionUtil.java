package hand.util;

import android.content.Context;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import hand.model.FuncNode;
import tt.toolkit.SPUtil;

public class FunctionUtil {

	public static void functionGrouped(List<FuncNode> funcNodeList, Context ctx) {
		Set<String> set = new HashSet<String>();
		Set<String> vehicleSet = new HashSet<String>();
		for (FuncNode node : funcNodeList) {
			String url = node.getUrl();
			String[] strArray = url.split("/");
			set.add(strArray[2]);
			if (strArray[2].equals("vehicle")) {
				vehicleSet.add(strArray[3]);
			}
		}
		SPUtil.putStringSet(ctx, "funcNodesp", set);
		SPUtil.putStringSet(ctx, "funcNodeVehicle", vehicleSet);
	}

}
