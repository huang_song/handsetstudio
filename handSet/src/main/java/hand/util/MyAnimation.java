package hand.util;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;

public class MyAnimation {
	
	public static void startAnimation(Context context, final View v, int resId) {
		Animation anim = AnimationUtils.loadAnimation(context, resId);
		v.startAnimation(anim);
		anim.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) { }

			@Override
			public void onAnimationRepeat(Animation animation) { }

			@Override
			public void onAnimationEnd(Animation animation) {
				
			}
		});
	}

}

