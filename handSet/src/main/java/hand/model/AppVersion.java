package hand.model;

public class AppVersion {

	private int versionCode;
	private String versionName;
	private boolean forceFlg; 	// 是否强制更新
	private String url; 		// 版本下载地址
	private String desc; 		// 更新详情
	private String platform; 	// android,ios
	private String type; 		// driver,train,store
	private String publishDate; // 创建时间

	public int getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(int versionCode) {
		this.versionCode = versionCode;
	}

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public boolean getForceFlg() {
		return forceFlg;
	}

	public void setForceFlg(boolean forceFlg) {
		this.forceFlg = forceFlg;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}

	@Override
	public String toString() {
		return "AppVersion [versionCode=" + versionCode + ", versionName=" + versionName + ", forceFlg=" + forceFlg
				+ ", url=" + url + ", desc=" + desc + ", platform=" + platform + ", type=" + type + ", publishDate="
				+ publishDate + "]";
	}

}
