package hand.model;

import java.io.Serializable;

public class AccessInfo implements Serializable {

	private String app_key;   // "62cd48dfb3945aa86293aac433733168"
	private String signature; // "7909BDE579B80C26E2B2500B9C4AFD9A" // --md5(appSecret)
	private String access_token;
	private String phone_num;

	public String getApp_key() {
		return app_key;
	}

	public void setApp_key(String app_key) {
		this.app_key = app_key;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public String getPhone_num() {
		return phone_num;
	}

	public void setPhone_num(String phone_num) {
		this.phone_num = phone_num;
	}

	@Override
	public String toString() {
		return "AccessInfo [app_key=" + app_key + ", signature=" + signature + ", access_token=" + access_token
				+ ", phone_num=" + phone_num + "]";
	}
	
}
