package hand.model;

import java.io.Serializable;

public class User_Info implements Serializable {

	private int id;
	private String name;
	private String orgName;
	private String phone;
	private String ttNo;
	private String avatarUrl;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTtNo() {
		return ttNo;
	}

	public void setTtNo(String ttNo) {
		this.ttNo = ttNo;
	}

	public String getAvatarUrl() {
		return avatarUrl;
	}

	public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}

	@Override
	public String toString() {
		return "User_Info [id=" + id + ", name=" + name + ", orgName=" + orgName + ", phone=" + phone + ", ttNo="
				+ ttNo + ", avatarUrl=" + avatarUrl + "]";
	}

}
