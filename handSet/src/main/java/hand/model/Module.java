package hand.model;

import java.util.ArrayList;

public class Module {

	private Long id;
	private String name;
	private String code;
	private ArrayList<FuncNode> funcNodes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ArrayList<FuncNode> getFuncNodes() {
		return funcNodes;
	}

	public void setFuncNodes(ArrayList<FuncNode> funcNodes) {
		this.funcNodes = funcNodes;
	}

}
