package hand.model;

import java.math.BigDecimal;

public class BillInfo {

	private int id;
	private int version;
	private String billNo; // 编号
	private String dealerBusinessOrgName; // 采购方
	private String productName;
	private double amount;
	private String unitName;
	private String vehicleNo; // 车号
	private String deliveryAt;

	private String storeOperateStatus;
	private BigDecimal storePackagesNumber;
	private int warehouseId;
	private BigDecimal storeSubductAmount;
	private BigDecimal storeAddAmount;
	
	
	private String driverName; 		 // 司机姓名
	private String driverPhone; 	 // 司机电话
	private String status;
	private String statusName; 		 // 状态
	
	private String productId;  		// 指定产品时用到
	private boolean virtualProduct;	// 需要指定产品
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}
	
	public String getDealerBusinessOrgName() {
		return dealerBusinessOrgName;
	}

	public void setDealerBusinessOrgName(String dealerBusinessOrgName) {
		this.dealerBusinessOrgName = dealerBusinessOrgName;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getDeliveryAt() {
		return deliveryAt;
	}

	public void setDeliveryAt(String deliveryAt) {
		this.deliveryAt = deliveryAt;
	}

	public String getStoreOperateStatus() {
		return storeOperateStatus;
	}

	public void setStoreOperateStatus(String storeOperateStatus) {
		this.storeOperateStatus = storeOperateStatus;
	}

	public BigDecimal getStorePackagesNumber() {
		return storePackagesNumber;
	}

	public void setStorePackagesNumber(BigDecimal storePackagesNumber) {
		this.storePackagesNumber = storePackagesNumber;
	}

	public int getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(int warehouseId) {
		this.warehouseId = warehouseId;
	}

	public BigDecimal getStoreSubductAmount() {
		return storeSubductAmount;
	}

	public void setStoreSubductAmount(BigDecimal storeSubductAmount) {
		this.storeSubductAmount = storeSubductAmount;
	}

	public BigDecimal getStoreAddAmount() {
		return storeAddAmount;
	}

	public void setStoreAddAmount(BigDecimal storeAddAmount) {
		this.storeAddAmount = storeAddAmount;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getDriverPhone() {
		return driverPhone;
	}

	public void setDriverPhone(String driverPhone) {
		this.driverPhone = driverPhone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public boolean isVirtualProduct() {
		return virtualProduct;
	}

	public void setVirtualProduct(boolean virtualProduct) {
		this.virtualProduct = virtualProduct;
	}
	
}
