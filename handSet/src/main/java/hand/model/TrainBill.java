package hand.model;

import java.math.BigDecimal;

public class TrainBill {

	private int id;
	private int version;
	private String trainNo; // 车号
	private String orderNum;
	private BigDecimal amount;
	private String productName;
	private String unitName;
	private String terminal;
	private BigDecimal packagesNumber;
	private String status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getTrainNo() {
		return trainNo;
	}

	public void setTrainNo(String trainNo) {
		this.trainNo = trainNo;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	public BigDecimal getPackagesNumber() {
		return packagesNumber;
	}

	public void setPackagesNumber(BigDecimal packagesNumber) {
		this.packagesNumber = packagesNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "TrainBill [id=" + id + ", version=" + version + ", trainNo=" + trainNo + ", orderNum=" + orderNum
				+ ", amount=" + amount + ", productName=" + productName + ", unitName=" + unitName + ", terminal="
				+ terminal + ", packagesNumber=" + packagesNumber + ", status=" + status + "]";
	}
	
}
