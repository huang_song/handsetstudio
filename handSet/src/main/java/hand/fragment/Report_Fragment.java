package hand.fragment;

import hand.ac.R;
import hand.adapter.Bill_Adapter_Report;
import hand.http.HandSetHttp;
import hand.http.HttpListener;
import hand.model.BillInfo;
import hand.util.Event_Refresh_Order;
import tt.toolkit.ToastUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import de.greenrobot.event.EventBus;
import tt.toolkit.JsonUtil;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class Report_Fragment extends BaseFragment implements OnRefreshListener<ListView>, OnLastItemVisibleListener {

	/** 1、EmptyWeighing待装车  2、Lading已装车 */
	public enum OrderStatus implements Serializable {
		EmptyWeighing, Lading
	}
	
	private TextView txt_empty_order;
	private PullToRefreshListView lv_bill;
	private Bill_Adapter_Report billAdapter;
	public List<BillInfo> billList;
	
	private int start = 0; // 第几条记录
	private int length = 10;
	private int currentPage;
	private int totalPages;
	
	private View rootView;
	public static LinearLayout lay_order_bar;
	private Context ctx;
	private OrderStatus orderStatus;
	private final int http_getBill = 1;

	// fragment 当自定义构造方法后，必须添加默认的无参构造
	public Report_Fragment() {

	}

	public Report_Fragment(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.report_fragment, container, false);
		ctx = rootView.getContext();
		txt_empty_order = (TextView) rootView.findViewById(R.id.txt_empty_order);
		lv_bill = (PullToRefreshListView)rootView.findViewById(R.id.lv_bill);
		initEvent();
		query(true, true);
		EventBus.getDefault().register(this, "onRefreshOrder", Event_Refresh_Order.class);
		return rootView;
	}
	
	private void initEvent() {
		billList = new ArrayList<BillInfo>();
		billAdapter = new Bill_Adapter_Report(ctx, billList);
		lv_bill.setAdapter(billAdapter);
	
		lv_bill.setOnRefreshListener(this); // 下拉刷新
		lv_bill.setOnLastItemVisibleListener(this); // 上拉加载
	}
	
	
	public void query(final boolean isRefresh, final boolean showProcess) {
		if (isRefresh) {
			start = 0;
		} else {
			start = start + length;
		}
		HashMap map = new HashMap<String, String>();
		map.put("start", start);
		map.put("length", length);
		map.put("appQueryType", orderStatus.name());

		HandSetHttp.getBill2(new HttpListener() {
			@Override
			public void onHttpSuccess(int requestCode, Object obj) {
				lv_bill.onRefreshComplete();
				if (isRefresh) {
					billList.clear();
				} 
				String result = (String) obj;
				String json = JsonUtil.getString(result, "datas");
				List<BillInfo> billListTemp = JsonUtil.json2BeanList(json, BillInfo.class);
				currentPage = JsonUtil.getInt(result, "currentPage");
			    totalPages = JsonUtil.getInt(result, "totalPages");
			    int totalDatas = JsonUtil.getInt(result, "totalDatas");
			    
				if (billListTemp != null && billListTemp.size() > 0) {
					billList.addAll(billListTemp);
				}
				lv_bill.setEmptyView(txt_empty_order);
				billAdapter.setDatas(billList);
				
				TextView txt_order_num = null;
				switch (orderStatus) {
				case EmptyWeighing:
					txt_order_num = (TextView) lay_order_bar.getChildAt(0).findViewById(R.id.txt_order0_num);
					break;
				case Lading:
					txt_order_num = (TextView) lay_order_bar.getChildAt(1).findViewById(R.id.txt_order1_num);
					break;
				}
				txt_order_num.setText(totalDatas + "");
			}
			
			@Override
			public void onHttpFail(int requestCode, String errorMsg) {
				lv_bill.onRefreshComplete();
				ToastUtil.showToast(ctx, errorMsg);
			}
		}, map, http_getBill, showProcess);
	}
	
	// 刷新订单列表
	public void onRefreshOrder(Event_Refresh_Order event) {
		query(true, false);
	}
	
	@Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		query(true, false);
	}

	@Override
	public void onLastItemVisible() {
		if (currentPage < totalPages) {
			query(false, false);
		}
	}
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
		EventBus.getDefault().unregister(this);
	}

}
