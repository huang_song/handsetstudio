package hand.fragment;

import java.math.BigDecimal;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import hand.ac.R;
import hand.http.HandSetHttp;
import hand.http.HttpListener;
import hand.model.TrainBill;
import tt.toolkit.JsonUtil;
import tt.toolkit.ToastUtil;
import tt.toolkit.view.SimpleDialog;
import tt.toolkit.view.SimpleDialog.OnPositiveClickListener;

public class MainTab_Train extends BaseFragment implements HttpListener {

	private EditText edit_search;
	private TextView text_search;
	private LinearLayout lay_bill_info;
	private TrainBill trainBill;
	
	private TextView txt_order_num;   	   // 编号
	private TextView txt_product_name;	   // 产品名称
	private TextView txt_terminal;         // 到站
	private TextView txt_entrust_quantity; // 委托数量
	EditText edit_package_num;  // 件数
	Button btn_confirm;
	private TextView txt_empty_train;
	
	private View rootView;
	private Context ctx;
	private final int http_getBill = 1;
	private final int http_Loading = 2;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.main_tab_train, container, false);
		rootView = view;
		ctx = view.getContext();
		initView();
		return rootView;
	}

	private void initView() {
		edit_search = (EditText) rootView.findViewById(R.id.edit_search);
		text_search = (TextView) rootView.findViewById(R.id.text_search);
		
		lay_bill_info = (LinearLayout) rootView.findViewById(R.id.lay_bill_info);
		txt_order_num = (TextView) rootView.findViewById(R.id.txt_order_num);
		txt_product_name = (TextView) rootView.findViewById(R.id.txt_product_name);
		txt_terminal = (TextView) rootView.findViewById(R.id.txt_terminal);
		txt_entrust_quantity = (TextView) rootView.findViewById(R.id.txt_entrust_quantity);
		edit_package_num = (EditText) rootView.findViewById(R.id.edit_package_num);
		btn_confirm = (Button) rootView.findViewById(R.id.btn_confirm);
		txt_empty_train = (TextView) rootView.findViewById(R.id.txt_empty_train);
		
		text_search.setOnClickListener(myClickListener);
		btn_confirm.setOnClickListener(myClickListener);
	}

	/** 查询数据 */
	private void query() {
		String trainNo = edit_search.getText().toString().trim();
		if (TextUtils.isEmpty(trainNo)) {
			ToastUtil.showToast(ctx, "请输入车号");
		} else {
			HandSetHttp.getTrainBill(this, trainNo, http_getBill);
		}
	}
	
	OnClickListener myClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.text_search:
				query();
				return;
			case R.id.btn_confirm:
				String package_nums = edit_package_num.getText().toString().trim();
				if (TextUtils.isEmpty(package_nums)) {
					ToastUtil.showToast(ctx, "请输入件数");
					return;
				}
				BigDecimal package_num = new BigDecimal(package_nums);
				showAlertDialog(trainBill, package_num);
				return;
			}
		}
	};
	
	// 确认装车对话框
	private void showAlertDialog(final TrainBill trainBill, final BigDecimal package_num) {
		SimpleDialog dialog = SimpleDialog.getInstance();
		dialog.setPositiveListener(new OnPositiveClickListener() {
			@Override
			public void onPositiveClick() {
				HandSetHttp.trainLoading(MainTab_Train.this, trainBill, package_num, http_Loading);
			}
		});
		dialog.showChooseDialog(ctx, "确认发货?", "取消", "确定", false);
	}

	@Override
	public void onHttpSuccess(int requestCode, Object obj) {
		switch (requestCode) {
		case http_getBill:
			String result = (String)obj;
			trainBill = JsonUtil.json2Bean(result, TrainBill.class);
			if (trainBill == null) {
				lay_bill_info.setVisibility(View.GONE);
				txt_empty_train.setVisibility(View.VISIBLE);
			} else {
				lay_bill_info.setVisibility(View.VISIBLE);
				txt_empty_train.setVisibility(View.GONE);
				txt_order_num.setText("订单号：" + trainBill.getOrderNum());
				txt_product_name.setText("产品名称：" + trainBill.getProductName());
				txt_terminal.setText("到站：" + trainBill.getTerminal());
				txt_entrust_quantity.setText("委托数量：" + trainBill.getAmount() + trainBill.getUnitName());
			}
			break;
		case http_Loading:
			ToastUtil.showToast(ctx, "发货成功");
			lay_bill_info.setVisibility(View.GONE);
			break;
		}
	}

	@Override
	public void onHttpFail(int requestCode, String errorMsg) {
		ToastUtil.showToast(ctx, errorMsg, true);
	}
	
}
