package hand.fragment;

import hand.ac.R;
import hand.adapter.Bill_Adapter_Car;
import hand.http.HandSetHttp;
import hand.http.HttpListener;
import hand.model.BillInfo;
import tt.toolkit.JsonUtil;
import tt.toolkit.ToastUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.speedata.libid2.IDInfor;
import com.speedata.libid2.IDManager;
import com.speedata.libid2.IDReadCallBack;
import com.speedata.libid2.IID2Service;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class MainTab_Car extends BaseFragment implements HttpListener {

	private Button btn_id2;
	private TextView txt_id_info;
	private TextView txt_error;
	
	private IID2Service iid2Service;
	private ListView lv_bill;
	private Bill_Adapter_Car billAdapter;
	private List<BillInfo> billList;
	private Context ctx;
	private View rootView;
	private final int http_getBill = 1;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.main_tab_car, container, false);
		ctx = rootView.getContext();
		initView();
		return rootView;
	}

	private void initView() {
		btn_id2 = (Button)rootView.findViewById(R.id.btn_id2);
		txt_id_info = (TextView) rootView.findViewById(R.id.txt_id_info);
		txt_error = (TextView) rootView.findViewById(R.id.txt_error);
		lv_bill = (ListView)rootView.findViewById(R.id.lv_bill);
		
		btn_id2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				btn_id2.setEnabled(false);
				initID();
			}
		});
		billList = new ArrayList<BillInfo>();
		billAdapter = new Bill_Adapter_Car(ctx, billList);
		lv_bill.setAdapter(billAdapter);
	}
	
	private void initID() {
		iid2Service = IDManager.getInstance();
		boolean result = false;
		try {
			result = iid2Service.initDev(ctx, new IDReadCallBack() {
				@Override
				public void callBack(IDInfor idInfo) {
					Message msg = new Message();
					msg.obj = idInfo;
					handler.sendMessage(msg);
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (result) {
			iid2Service.getIDInfor(false);
		} else {
			btn_id2.setEnabled(true);
			txt_id_info.setVisibility(View.GONE);
			txt_error.setVisibility(View.VISIBLE);
			txt_error.setText("二代证模块初始化失败！");
			lv_bill.setVisibility(View.GONE);
		}
	}
	
	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			try {
				iid2Service.releaseDev();
			} catch (IOException e) {
				e.printStackTrace();
			}
			IDInfor idInfo = (IDInfor) msg.obj;
			if (idInfo.isSuccess()) {
				playMusic();
				String[] ids = idInfo.getNum().split("\u0000");
				String idCard = "";
				for (String str : ids) {
					idCard += str;
				}
				txt_id_info.setVisibility(View.VISIBLE);
				txt_id_info.setText("姓名：" + idInfo.getName() + "\n身份证号：" + idInfo.getNum() );
//				idCard = "371481199304280319"; //小朱
//				idCard = "370785199211026523"; //晓悦
//				idCard = "34222119901012153X"; //黄宋
//				idCard = "130227198401295214"; //周伟
//				idCard = "132926198010022431"; //洪元
				HandSetHttp.getBill(MainTab_Car.this, idCard, http_getBill);
			} else {
				btn_id2.setEnabled(true);
				txt_id_info.setVisibility(View.GONE);
				txt_error.setVisibility(View.VISIBLE);
				txt_error.setText(idInfo.getErrorMsg());
				lv_bill.setVisibility(View.GONE);
			}
		}
	};
	
	private void playMusic() {
		final MediaPlayer mediaPlayer = MediaPlayer.create(ctx, R.raw.ring);
		mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		mediaPlayer.start();
		mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				mediaPlayer.release();
			}
		});
	}
	
	@Override
	public void onHttpSuccess(int requestCode, Object obj) {
		switch (requestCode) {
		case http_getBill:
			btn_id2.setEnabled(true);
			String result = (String)obj;
			billList = JsonUtil.json2BeanList(result, BillInfo.class);
			if(billList.size() == 0) {
				txt_error.setVisibility(View.VISIBLE);
				txt_error.setText("当前司机无装车单");
				lv_bill.setVisibility(View.GONE);
			} else {
				txt_error.setVisibility(View.GONE);
				lv_bill.setVisibility(View.VISIBLE);
				billAdapter.setDatas(billList);
			}
			break;
		}
	}

	@Override
	public void onHttpFail(int requestCode, String errorMsg) {
		btn_id2.setEnabled(true);
		ToastUtil.showToast(ctx, errorMsg, true);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		try {
			if (iid2Service != null) {
				iid2Service.releaseDev();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
