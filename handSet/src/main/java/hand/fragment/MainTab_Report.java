package hand.fragment;

import hand.ac.R;
import hand.adapter.FragmentAdapter;
import hand.fragment.Report_Fragment.OrderStatus;
import java.util.ArrayList;
import java.util.List;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class MainTab_Report extends BaseFragment implements OnPageChangeListener {

	private View rootView;
	public LinearLayout lay_order_bar;
	private ViewPager vp;
	public List<Fragment> fragmentList;
	private int prePosition;	
	private Context ctx;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.main_tab_report, container, false);
		ctx = rootView.getContext();
		lay_order_bar = (LinearLayout) rootView.findViewById(R.id.lay_order_bar);
		Report_Fragment.lay_order_bar = lay_order_bar;
		vp = (ViewPager) rootView.findViewById(R.id.viewpager_myorder);
		vp.setOffscreenPageLimit(2);
		initEvent();
		return rootView;
	}
	
	private void initEvent() {
		fragmentList = new ArrayList<Fragment>();
		final Report_Fragment fragment1 = new Report_Fragment(OrderStatus.EmptyWeighing);
		final Report_Fragment fragment2 = new Report_Fragment(OrderStatus.Lading);
		fragmentList.add(fragment1);
		fragmentList.add(fragment2);
//		FragmentAdapter adapter = new FragmentAdapter(getSupportFragmentManager(), fragmentList);
		FragmentAdapter adapter = new FragmentAdapter(getChildFragmentManager(), fragmentList);
		vp.setAdapter(adapter);
		vp.setOnPageChangeListener(this);
		vp.setOffscreenPageLimit(2);
		int count = lay_order_bar.getChildCount();
		for (int i = 0; i < count; i++) {
			RelativeLayout child = (RelativeLayout) lay_order_bar.getChildAt(i);
			child.setTag(i);
			child.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					int tag = (Integer) v.getTag();
					vp.setCurrentItem(tag);
					if (tag == 0 ) {
						fragment1.query(true, false);
					} else {
						fragment2.query(true, false);
					}
				}
			});
		}
		lay_order_bar.getChildAt(0).setSelected(true);
		vp.setCurrentItem(0);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onPageScrollStateChanged(int position) { }
	
	@Override
	public void onPageScrolled(int position, float arg1, int arg2) { }
	
	@Override
	public void onPageSelected(int position) {
		lay_order_bar.getChildAt(prePosition).setSelected(false);
		lay_order_bar.getChildAt(position).setSelected(true);
		prePosition = position;
	}
	
}
