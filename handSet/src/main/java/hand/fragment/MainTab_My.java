package hand.fragment;

import hand.ac.R;
import hand.app.MyConstant;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MainTab_My extends BaseFragment {

	private TextView txt_user_name;    // 用户名
	private TextView txt_phone_number; // 手机号
	private View rootView;
	private Context ctx;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.main_tab_my, container, false);
		rootView = view;
		ctx = view.getContext();
		initView();
		return rootView;
	}

	private void initView() {
		txt_user_name = (TextView) rootView.findViewById(R.id.txt_user_name);
		txt_phone_number = (TextView) rootView.findViewById(R.id.txt_phone_number);
		txt_user_name.setText("用户名：" + MyConstant.userInfo.getName());
		txt_phone_number.setText("手机号：" + MyConstant.userInfo.getPhone());
	}
	
}
