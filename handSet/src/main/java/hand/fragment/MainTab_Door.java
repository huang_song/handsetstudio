package hand.fragment;

import hand.ac.R;
import hand.adapter.Bill_Adapter_Door;
import hand.http.HandSetHttp;
import hand.http.HttpListener;
import hand.model.BillInfo;
import tt.toolkit.ToastUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import tt.toolkit.JsonUtil;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechSynthesizer;
import com.speedata.libid2.IDInfor;
import com.speedata.libid2.IDManager;
import com.speedata.libid2.IDReadCallBack;
import com.speedata.libid2.IID2Service;
import android.content.Context;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class MainTab_Door extends BaseFragment implements HttpListener {
	
	private Button btn_id2;
	private TextView txt_msg;
	private TextView txt_id_info;
	private TextView txt_error;
	
	private IID2Service iid2Service;
	private ListView lv_bill;
	private Bill_Adapter_Door billAdapter;
	private List<BillInfo> billList;
	
	private SpeechSynthesizer mTts;	// 语音合成对象
	private View rootView;
	private Context ctx;
	private final int http_getBill = 1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.main_tab_door, container, false);
		rootView = view;
		ctx = view.getContext();
		initView();
		mTts = SpeechSynthesizer.createSynthesizer(ctx, null);	// 初始化语音合成对象
		return rootView;
	}
	
	private void initView() {
		btn_id2 = (Button) rootView.findViewById(R.id.btn_id2);
		txt_msg = (TextView) rootView.findViewById(R.id.txt_msg);
		txt_id_info = (TextView) rootView.findViewById(R.id.txt_id_info);
		
		txt_error = (TextView) rootView.findViewById(R.id.txt_error);
		btn_id2.setOnClickListener( new OnClickListener() {
			@Override
			public void onClick(View v) {
				btn_id2.setEnabled(false);
				txt_msg.setVisibility(View.GONE);
				initID();
			}
		});
		
		lv_bill = (ListView) rootView.findViewById(R.id.lv_bill);
		billList = new ArrayList<BillInfo>();
		billAdapter = new Bill_Adapter_Door(ctx, billList);
		lv_bill.setAdapter(billAdapter);
	}
	
	private void initID() {
		iid2Service = IDManager.getInstance();
		boolean result = false;
		try {
			result = iid2Service.initDev(ctx, new IDReadCallBack() {
				@Override
				public void callBack(IDInfor idInfo) {
					Message msg = new Message();
					msg.obj = idInfo;
					handler.sendMessage(msg);
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (result) {
			iid2Service.getIDInfor(false);
		} else {
			btn_id2.setEnabled(true);
			txt_id_info.setVisibility(View.GONE);
			txt_error.setVisibility(View.VISIBLE);
			txt_error.setText("二代证模块初始化失败！");
			lv_bill.setVisibility(View.GONE);
		}
	}
	
	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			try {
				iid2Service.releaseDev();
			} catch (IOException e) {
				e.printStackTrace();
			}
			IDInfor idInfo = (IDInfor) msg.obj;
			if (idInfo.isSuccess()) {
				playMusic();
				String[] ids = idInfo.getNum().split("\u0000");
				String idCard = "";
				for (String str : ids) {
					idCard += str;
				}
				txt_id_info.setVisibility(View.VISIBLE);
				txt_id_info.setText("姓名：" + idInfo.getName() + "\n身份证号：" + idInfo.getNum() );
//				idCard = "371481199304280319"; //小朱
//				idCard = "370785199211026523"; //晓悦
//				idCard = "34222119901012153X"; //黄宋
//				idCard = "130227198401295214"; //周伟
//				idCard = "132926198010022431"; //洪元
				HandSetHttp.getDoorBill(MainTab_Door.this, idCard, http_getBill);
			} else {
				btn_id2.setEnabled(true);
				txt_id_info.setVisibility(View.GONE);
				txt_error.setVisibility(View.VISIBLE);
				txt_error.setText(idInfo.getErrorMsg());
				lv_bill.setVisibility(View.GONE);
			}
		}
	};
	
	private void playMusic() {
		final MediaPlayer mediaPlayer = MediaPlayer.create(ctx, R.raw.ring);
		mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
		mediaPlayer.start();
		mediaPlayer.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				mediaPlayer.release();
			}
		});
	}
	
    public void play(String text, String status) {
		mTts.setParameter(SpeechConstant.PARAMS, null);	// 清空参数
		// 根据合成引擎设置相应参数
		mTts.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD);
		txt_msg.setVisibility(View.VISIBLE);
		txt_msg.setText(text);
		if ("Error".equals(status) || "muchProduct".equals(status)) {
			txt_msg.setTextColor(Color.RED);
			mTts.setParameter(SpeechConstant.VOICE_NAME, "xiaoyu");
		} else {
			txt_msg.setTextColor(Color.GREEN);
			mTts.setParameter(SpeechConstant.VOICE_NAME, "xiaoyan");
		}
		mTts.setParameter(SpeechConstant.SPEED, "50");				 // 设置语速
		mTts.setParameter(SpeechConstant.PITCH, "50");				 // 设置音调
		mTts.setParameter(SpeechConstant.VOLUME, "100");			 // 设置音量
		mTts.setParameter(SpeechConstant.STREAM_TYPE, "2"); 		 // 设置播放器音频流类型
		mTts.setParameter(SpeechConstant.KEY_REQUEST_FOCUS, "true"); // 设置播放合成音频打断音乐播放，默认为true
		// 0：自动，不确定时按值发音； 1：按值发音（一百二十三）； 2：按串发音（一二三）； 3：自动，不确定时按串发音
		mTts.setParameter("rdn","2");
		int code = mTts.startSpeaking(text, null);
    }
    
	@Override
	public void onHttpSuccess(int requestCode, Object obj) {
		switch (requestCode) {
		case http_getBill:
			btn_id2.setEnabled(true);
			String result = (String) obj;
			String json = JsonUtil.getString(result, "data");
			billList = JsonUtil.json2BeanList(json, BillInfo.class);
			String msg = JsonUtil.getString(result, "msg");
			String status = JsonUtil.getString(result, "status");
			play(msg, status);
			if(billList.size() == 0) {
				txt_error.setVisibility(View.VISIBLE);
				txt_error.setText("当前司机无装车单");
				lv_bill.setVisibility(View.GONE);
			} else {
				txt_error.setVisibility(View.GONE);
				lv_bill.setVisibility(View.VISIBLE);
				billAdapter.setDatas(billList);
			}
			break;
		}
	}

	@Override
	public void onHttpFail(int requestCode, String errorMsg) {
		btn_id2.setEnabled(true);
		ToastUtil.showToast(ctx, errorMsg, true);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		try {
			if (iid2Service != null) {
				iid2Service.releaseDev();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		mTts.stopSpeaking();
		mTts.destroy(); // 退出时释放连接
	}
	
}
