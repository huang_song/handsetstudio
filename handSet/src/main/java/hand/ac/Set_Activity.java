package hand.ac;

import hand.app.UpdateManager;
import hand.http.HttpListener;
import hand.http.UserHttp;
import hand.util.MyUtil;
import tt.toolkit.TTUtil;
import tt.toolkit.ToastUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Set_Activity extends BaseActivity implements HttpListener {

	private LinearLayout lay_update_phone; 	  // 修改手机号
	private LinearLayout lay_update_password; // 修改密码
	private LinearLayout lay_current_version; // 当前版本
	private TextView txt_version;
	private Button btn_exit;
	private final int http_loginOut = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.set_activity);
		context = Set_Activity.this;
		initView();
		initEvent();
	}

	private void initView() {
		lay_update_phone = (LinearLayout) findViewById(R.id.lay_update_phone);
		lay_update_password = (LinearLayout) findViewById(R.id.lay_update_password);
		lay_current_version = (LinearLayout) findViewById(R.id.lay_current_version);
		txt_version = (TextView) findViewById(R.id.txt_version);
		btn_exit = (Button) findViewById(R.id.btn_exit);
	}

	private void initEvent() {
		SetTitle("设置");
		showReturn(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		txt_version.setText("v" + MyUtil.getMyAppVersion(context).getVersionName());
		
		lay_update_phone.setOnClickListener(MyClickListener);
		lay_update_password.setOnClickListener(MyClickListener);
		lay_current_version.setOnClickListener(MyClickListener);
		btn_exit.setOnClickListener(MyClickListener);
	}

	OnClickListener MyClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent();
			switch (v.getId()) {
			case R.id.lay_update_phone:
				intent.setClass(context, Update_Phone.class);
				startActivity(intent);
				break;
			case R.id.lay_update_password:
				intent.setClass(context, Update_Pwd.class);
				startActivity(intent);
				break;
			case R.id.lay_current_version:
				if (TTUtil.isNetConnected(context)) {
					UpdateManager ud = new UpdateManager(context);
					ud.checkUpdateInfo(true);
				} else {
					ToastUtil.showToast(context, "请先连接手机网络");
				}
				break;
			case R.id.btn_exit:
				loginOut();
				UserHttp.loginOut(Set_Activity.this, http_loginOut);
				break;
			}
			
		}
	};

	@Override
	public void onHttpSuccess(int requestCode, Object obj) {
		switch (requestCode) {
		case http_loginOut:

			break;
		}
	}

	@Override
	public void onHttpFail(int requestCode, String errorMsg) {
		
	}

}
