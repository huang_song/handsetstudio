package hand.ac;

import hand.app.UpdateManager;
import hand.fragment.MainTab_Door;
import hand.fragment.MainTab_My;
import tt.toolkit.TTUtil;
import tt.toolkit.SPUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class Index_Activity_Door extends BaseActivity {
	
	private MainTab_Door tab01;
	private MainTab_My tab02;
	private RelativeLayout lay_foot_index;
	private RelativeLayout lay_foot_my;
	private Button btn_foot_index; // 出入厂
	private Button btn_foot_my;    // 我的

	private List<Fragment> fragments = new ArrayList<Fragment>();
	FragmentManager fragmentManager;
	FragmentTransaction fragmentTransaction;
	private ImageView img_other_common;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.index_activity_door);
		tt.util.ActivityCollector.addActivity(this);
		context = this;
		initView();
		initData();
	}
	
	private void initView() {
		Set<String> set = SPUtil.getStringSet(context, "funcNodesp");
		if (set.size() > 1) {
			showReturn(new OnClickListener() {
				@Override
				public void onClick(View v) {
					finish();
				}
			});
		}
		lay_foot_index = (RelativeLayout) findViewById(R.id.lay_foot_index);
		lay_foot_my = (RelativeLayout) findViewById(R.id.lay_foot_my);
		btn_foot_index = (Button) findViewById(R.id.btn_foot_index);
		btn_foot_my = (Button) findViewById(R.id.btn_foot_my);
		lay_foot_index.setOnClickListener(MyClickListener);
		lay_foot_my.setOnClickListener(MyClickListener);
		img_other_common = (ImageView) findViewById(R.id.img_other_common);
		img_other_common.setOnClickListener(MyClickListener);
	}

	private void initData() {
		tab01 = new MainTab_Door();
		tab02 = new MainTab_My();
		fragments.add(tab01);
		fragments.add(tab02);

		fragmentManager = getSupportFragmentManager();
		fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.add(R.id.fragment_container, tab01);
		fragmentTransaction.add(R.id.fragment_container, tab02);
		fragmentTransaction.show(tab01).hide(tab02);
		fragmentTransaction.commitAllowingStateLoss();
		switchFragment(0);/////////////////////////////
	}

	OnClickListener MyClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.lay_foot_index:
				switchFragment(0);
				break;
			case R.id.lay_foot_my:
				switchFragment(1);
				break;
			case R.id.img_other_common:
				Intent intent = new Intent(context, Set_Activity.class);
				startActivity(intent);
				break;
			}
		}
	};

	public void switchFragment(int index) {
		if(fragments.get(index).isVisible()) {
			return;
		}
		changeFragment(index);
		img_other_common.setVisibility(View.GONE);
		
		switch (index) {
		case 0:
			SetTitle("出入厂");
			btn_foot_index.setTextColor(getResources().getColor(R.color.blue));
			break;
		case 1:
			SetTitle("个人中心");
			btn_foot_my.setTextColor(getResources().getColor(R.color.blue));
			img_other_common.setVisibility(View.VISIBLE);
			break;
		}
	}
	
	private void changeFragment(int index) {
		btn_foot_index.setTextColor(getResources().getColor(R.color.text_gray2));
		btn_foot_my.setTextColor(getResources().getColor(R.color.text_gray2));
		
		fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		for (int i = 0; i < fragments.size(); i++) { // 试图解决fragment切换时的重叠
			fragmentTransaction.hide(fragments.get(i));
		}
		fragmentTransaction.show(fragments.get(index));
		fragmentTransaction.commit();
	}

//	@Override
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//		if (keyCode == KeyEvent.KEYCODE_BACK) {
//			Intent intent = new Intent(Intent.ACTION_MAIN);
//			intent.addCategory(Intent.CATEGORY_HOME);
//			startActivity(intent);
//			return false;
//		} else {
//			return super.onKeyDown(keyCode, event);
//		}
//	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// 里面什么都没有，试图解决fragement的重叠
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (TTUtil.isNetConnected(context)) {
			UpdateManager ud = new UpdateManager(context);
			ud.checkUpdateInfo(false);
		}
	}
	
}
