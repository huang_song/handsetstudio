package hand.ac;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import tt.toolkit.SPUtil;

public class BaseActivity extends FragmentActivity {

	private LinearLayout lay_return;
	private TextView txt_title;
	private LinearLayout lay_other;
	private TextView txt_other;
	private ImageView img_other;
	public Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		// 设置没有title
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	}

	private void initView() {
		if (lay_return == null) {
			lay_return = (LinearLayout) findViewById(R.id.layout_return_common);
			txt_title = (TextView) findViewById(R.id.txt_title_common);
			lay_other = (LinearLayout) findViewById(R.id.layout_other_common);
			txt_other = (TextView) findViewById(R.id.txt_other_common);
			img_other = (ImageView) findViewById(R.id.img_other_common);
			
			lay_return.setVisibility(View.INVISIBLE);
		}
	}


	/**
	 * 标题栏左侧返回箭头的监听事件
	 * 
	 * @param listener
	 */
	public void showReturn(OnClickListener listener) {
		initView();
		if (lay_return != null) {
			lay_return.setOnClickListener(listener);
			lay_return.setVisibility(View.VISIBLE);
		}
	}

	public void SetTitle(String title) {
		initView();
		if (txt_title != null) {
			txt_title.setText(title);
		}
	}


	/**
	 * 标题栏右侧内容的监听事件
	 * 
	 * @param listener
	 */
	public void setCallBack_Other(OnClickListener listener) {
		initView();
		if (lay_other != null) {
			lay_other.setOnClickListener(listener);
		}
	}

	/** 显示标题栏右侧的内容 */
	public void showOther(String otherName) {
		initView();
		if (otherName != null && otherName.length() > 0) {
			txt_other.setVisibility(View.VISIBLE);
			img_other.setVisibility(View.GONE);
			txt_other.setText(otherName);
		}
	}
	
	public void loginOut() {
		SPUtil.remove(context, "userMD5");
		SPUtil.remove(context, "accessInfo");
		SPUtil.remove(context, "userInfo");
		SPUtil.remove(context, "funcNodesp");
		SPUtil.remove(context, "funcNodeVehicle");
		Intent intent = new Intent(context, Login_Activity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		finish();
		tt.util.ActivityCollector.finishAll();
	}

}
