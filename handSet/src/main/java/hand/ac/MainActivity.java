package hand.ac;

import java.util.List;
import java.util.Set;
import hand.app.MyConstant;
import hand.http.HttpListener;
import hand.http.HttpUrl;
import hand.http.UserHttp;
import hand.model.AccessInfo;
import hand.model.Module;
import hand.model.User_Info;
import hand.util.FunctionUtil;
import hand.util.MyAnimation;
import tt.toolkit.SPUtil;
import tt.toolkit.ToastUtil;
import tt.toolkit.JsonUtil;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends BaseActivity implements HttpListener {

	private TextView txt_http_url;
	private ImageView load_image;
	private final int http_function = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		context = this;
		txt_http_url = (TextView) findViewById(R.id.txt_http_url);
		load_image = (ImageView) findViewById(R.id.load_image);
		if (!HttpUrl.json_root.equals("https://www.zhiyunlanhai.com/es/")) {
			txt_http_url.setVisibility(View.VISIBLE);
			txt_http_url.setText("App连接地址：" + HttpUrl.json_root);
		}
		MyAnimation.startAnimation(context, load_image, R.anim.load_anim);
		goIndex();
	}

	void goIndex() {
		new Handler().postDelayed(new Runnable() {
			public void run() {
				String accessJson = SPUtil.getString(context, "accessInfo", null);
				String userJson = SPUtil.getString(context, "userInfo", null);
				if (accessJson == null) {
					Intent intent = new Intent(context, Login_Activity.class);
					startActivity(intent);
					finish();
				} else {
					MyConstant.accessInfo = JsonUtil.json2Bean(accessJson, AccessInfo.class);
					MyConstant.userInfo = JsonUtil.json2Bean(userJson, User_Info.class);
					UserHttp.getFunction(MainActivity.this, http_function);
				}
			}
		}, 2000);
	}
	
	@Override
	public void onHttpSuccess(int requestCode, Object obj) {
		String result = (String)obj;
		String json = JsonUtil.getString(result, "modules");
		List<Module> moduleList = JsonUtil.json2BeanList(json, Module.class);
		for (Module module : moduleList) {
			if (module.getName().equals("手持机")) {
				FunctionUtil.functionGrouped(module.getFuncNodes(), context);
				break;
			}
		}

		Set<String> set = SPUtil.getStringSet(context, "funcNodesp");
		Intent intent = new Intent();
		if(set == null) {
			intent.setClass(context, Login_Activity.class);
		} else if (set.size() == 1) {
			String funcNode = set.toString();
			if (funcNode.equals("[doorkeeper]")) {
				intent.setClass(context, Index_Activity_Door.class);
			} else if (funcNode.equals("[vehicle]")) {
				intent.setClass(context, Index_Activity_Car.class);
			} else {
				intent.setClass(context, Index_Activity_Train.class);
			}
		} else {
			intent.setClass(context, Function_Activity.class);
		}
		startActivity(intent);
		finish();
	}

	@Override
	public void onHttpFail(int requestCode, String errorMsg) {
		ToastUtil.showToast(context, errorMsg);
		Intent intent = new Intent(context, Login_Activity.class);
		startActivity(intent);
		finish();
	}

}
