package hand.ac;

import hand.app.MyConstant;
import hand.app.UpdateManager;
import hand.model.Module;
import hand.util.FunctionUtil;
import tt.toolkit.TTUtil;
import tt.toolkit.SPUtil;
import tt.toolkit.ToastUtil;

import java.util.List;
import java.util.Map;
import java.util.Set;

import hand.http.HttpListener;
import hand.http.UserHttp;
import hand.model.User_Info;
import hand.util.MyUtil;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import tt.toolkit.JsonUtil;

public class Login_Activity extends BaseActivity implements HttpListener {

	private EditText edit_phone;
	private EditText edit_pwd;
	private String phone;
	private String password;
	
	private Button btn_login;
	private TextView txt_forgot_pwd;
	private String userMD5;
	private final int http_getMD5 = 1;
	private final int http_login = 2;
	private final int http_function = 3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);
		context = this;
		initView();
		initEvent();
		if (TTUtil.isNetConnected(context)) {
			UpdateManager ud = new UpdateManager(context);
			ud.checkUpdateInfo(false);
		}
	}

	private void initView() {
		edit_phone = (EditText) findViewById(R.id.edit_login_phone);
		edit_pwd = (EditText) findViewById(R.id.edit_login_password);
		btn_login = (Button) findViewById(R.id.btn_login);
		txt_forgot_pwd = (TextView) findViewById(R.id.txt_forgot_pwd);
		SetTitle("登录");
	}

	private void initEvent() {
		btn_login.setOnClickListener(myClickListener);
		txt_forgot_pwd.setOnClickListener(myClickListener);
		MyUtil.setUnderLine(txt_forgot_pwd);
	}

	OnClickListener myClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_login:
				if (checkAccount()) {
					MyConstant.initAccessInfo();
					MyConstant.accessInfo.setPhone_num(phone);
					UserHttp.getUserMD5(Login_Activity.this, http_getMD5);
				}
				break;
			case R.id.txt_forgot_pwd:
				MyConstant.initAccessInfo();
				Intent intent = new Intent(context, ResetPwd_Activity.class);
				startActivity(intent);
				break;
			}
		}
	};

	/** 检验手机号，密码 */
	private boolean checkAccount() {
		phone = edit_phone.getText().toString().trim();
		password = edit_pwd.getText().toString().trim();
		
		if (TextUtils.isEmpty(phone)) {
			ToastUtil.showToast(context, "请输入手机号!");
			return false;
		}
		if (TextUtils.isEmpty(password)) {
			ToastUtil.showToast(context, "请输入密码!");
			return false;
		}
		return true;
	}

	@Override
	public void onHttpSuccess(int requestCode, Object obj) {
		String result = (String)obj;
		switch (requestCode) {
		case http_getMD5:
			userMD5 = JsonUtil.getString(result, "userMD5");;
			String signature =  MyUtil.stringToMD5(MyConstant.appSecret + MyUtil.stringToMD5(phone + password + userMD5));
			MyConstant.accessInfo.setSignature(signature);
			UserHttp.loginRquest(Login_Activity.this, http_login);
			break;
		case http_login:
			Map map = JsonUtil.json2Map(result);;
			String signature2 =  MyUtil.stringToMD5(MyConstant.appSecret +  "&" + map.get("accessTokenSecret"));
			MyConstant.accessInfo.setSignature(signature2);
			MyConstant.accessInfo.setAccess_token((String) map.get("accessToken"));
			MyConstant.userInfo = JsonUtil.json2Bean(map.get("userInfo").toString(), User_Info.class);
			
			SPUtil.putString(context, "userMD5", userMD5);
			SPUtil.putString(context, "accessInfo", JsonUtil.bean2Json(MyConstant.accessInfo));
			SPUtil.putString(context, "userInfo", map.get("userInfo").toString());
			UserHttp.getFunction(Login_Activity.this, http_function);
			break;
		case http_function:
            String json = JsonUtil.getString(result, "modules");
			List<Module> moduleList = JsonUtil.json2BeanList(json, Module.class);
			for (Module module : moduleList) {
				if (module.getName().equals("手持机")) {
					FunctionUtil.functionGrouped(module.getFuncNodes(), context);
					break;
				}
			}

			Set<String> set = SPUtil.getStringSet(context, "funcNodesp");
			Intent intent = new Intent();
			if(set == null) {
				ToastUtil.showToast(context, "该账号无权登录此系统", true);
				break;
			} else if (set.size() == 1) {
				String funcNode = set.toString();
				if (funcNode.equals("[doorkeeper]")) {
					intent.setClass(context, Index_Activity_Door.class);
				} else if (funcNode.equals("[vehicle]")) {
					intent.setClass(context, Index_Activity_Car.class);
				} else {
					intent.setClass(context, Index_Activity_Train.class);
				}
			} else {
				intent.setClass(context, Function_Activity.class);
			}
			startActivity(intent);
			finish();
			break;
		}
	}

	@Override
	public void onHttpFail(int requestCode, String errorMsg) {
		ToastUtil.showToast(context, errorMsg);
	}

}
