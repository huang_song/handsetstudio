package hand.ac;

import java.util.HashMap;
import hand.app.MyConstant;
import hand.http.HttpListener;
import hand.http.UserHttp;
import hand.util.MyUtil;
import tt.toolkit.JsonUtil;
import tt.toolkit.ToastUtil;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class ResetPwd_Activity extends BaseActivity implements HttpListener {

	private EditText edit_phone;
	private EditText edit_code; // 验证码
	private Button btn_getCode;
	private EditText edit_password1;
	private EditText edit_password2;
	private Button btnSubmit;
	private String phone;
	private String verifyCode;
	private String password1;
	private String password2;
	private String userMD5;
	private static long current_time = 0;
	
	private final int http_getMD5 = 1;
	private final int http_getCode = 2;
	private final int http_submit = 3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reset_pwd_activity);
		context = ResetPwd_Activity.this;
		initView();
		initListener();
	}
	
	private Handler timehandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			long nowDate = System.currentTimeMillis();
			long calc = (nowDate - current_time) / 1000;
			if (calc >= 60) {
				btn_getCode.setClickable(true);
				btn_getCode.setText("获取验证码");
				btn_getCode.setTextColor(context.getResources().getColor(R.color.text_white));
				timehandler.removeMessages(0);
			} else {
				btn_getCode.setText((60 - calc) + "");
				btn_getCode.setTextColor(context.getResources().getColor(R.color.text_gray3));
				timehandler.sendEmptyMessageDelayed(0, 1000);
			}
		}
	};

	private void initView() {
		edit_phone = (EditText) findViewById(R.id.et_mobile);
		edit_code = (EditText) findViewById(R.id.edit_verify_code);
		btn_getCode = (Button) findViewById(R.id.button_get_code);
		edit_password1 = (EditText) findViewById(R.id.edit_new_password);
		edit_password2 = (EditText) findViewById(R.id.edit_new_password2);
		btnSubmit = (Button) findViewById(R.id.button_submit);
		SetTitle("重置密码");
	}

	private void initListener() {
		showReturn(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		btn_getCode.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (checkPhone()) {
					btn_getCode.setClickable(false);
					UserHttp.getUserMD5(ResetPwd_Activity.this, http_getMD5);
				}
			}
		});

		btnSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (checkUserInfo()) {
					HashMap map = new HashMap<String, Object>();
					map.put("check_code", verifyCode);
					map.put("password", MyUtil.stringToMD5(phone + password1 + userMD5));
					UserHttp.resetPwd(ResetPwd_Activity.this, map, http_submit);
				}
			}
		});
	}

	private boolean checkPhone() {
		phone = edit_phone.getText().toString().trim();
		if (TextUtils.isEmpty(phone)) {
			ToastUtil.showToast(context, "请输入手机号");
			return false;
		}
		MyConstant.accessInfo.setPhone_num(phone);
		return true;
	}

	private boolean checkUserInfo() {
		verifyCode = edit_code.getText().toString().trim();
		password1 = edit_password1.getText().toString().trim();
		password2 = edit_password2.getText().toString().trim();
		if (TextUtils.isEmpty(verifyCode)) {
			ToastUtil.showToast(context, "请输入短信验证码");
			return false;
		}
		if (TextUtils.isEmpty(password1)) {
			ToastUtil.showToast(context, "请输入新密码");
			return false;
		}
		if (TextUtils.isEmpty(password2)) {
			ToastUtil.showToast(context, "请输入确认密码");
			return false;
		}
		if (!password1.equals(password2)) {
			ToastUtil.showToast(context, "两次密码输入的不一致");
			return false;
		}
		checkPhone();
		return true;
	}

	@Override
	public void onHttpSuccess(int requestCode, Object obj) {
		switch (requestCode) {
		case http_getMD5:
			String result = (String)obj;
			userMD5 = JsonUtil.getString(result, "userMD5");;
			UserHttp.getResetPwdCode(ResetPwd_Activity.this, http_getCode);
			current_time = System.currentTimeMillis();
			timehandler.sendEmptyMessage(0);
			break;
		case http_getCode:
			
			break;
		case http_submit:
			ToastUtil.showToast(context, "重置密码成功", true);
			finish();
			break;
		}
	}

	@Override
	public void onHttpFail(int requestCode, String errorMsg) {
		switch (requestCode) {
		case http_getMD5:
		case http_getCode:
		case http_submit:
			ToastUtil.showToast(context, errorMsg);
			break;
		}
	}

}
