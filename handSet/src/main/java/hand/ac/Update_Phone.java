package hand.ac;

import java.util.HashMap;

import hand.app.MyConstant;
import hand.http.HttpListener;
import hand.http.UserHttp;
import hand.util.MyUtil;
import tt.toolkit.SPUtil;
import tt.toolkit.ToastUtil;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Update_Phone extends BaseActivity implements HttpListener {

	private TextView txt_phone;
	private EditText edit_pwd;
	private EditText edit_new_phone;
	private EditText edit_check_code; // 验证码
	private Button btn_get_code;
	private Button btn_submit;
	
	private String old_phone;
	private String old_pwd;
	private String new_phone;
	private String checkCode;
	private static long current_time = 0;
	private final int http_getPhoneCode = 1;
	private final int http_updatePhone = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.update_phone);
		context = Update_Phone.this;
		initView();
		initEvent();
	}
	
	private Handler timeHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			long nowDate = System.currentTimeMillis();
			long calc = (nowDate - current_time) / 1000;
			if (calc >= 60) {
				btn_get_code.setClickable(true);
				btn_get_code.setText("获取验证码");
				btn_get_code.setTextColor(context.getResources().getColor(R.color.text_white));
			} else {
				btn_get_code.setText((60 - calc) + "");
				btn_get_code.setTextColor(context.getResources().getColor(R.color.text_gray3));
				timeHandler.sendEmptyMessageDelayed(0, 1000);
			}
		}
	};

	private void initView() {
		txt_phone = (TextView) findViewById(R.id.txt_phone);
		edit_pwd = (EditText) findViewById(R.id.edit_pwd);
		edit_new_phone = (EditText) findViewById(R.id.edit_new_phone);
		edit_check_code = (EditText) findViewById(R.id.edit_check_code);
		
		btn_get_code = (Button) findViewById(R.id.btn_get_code);
		btn_submit = (Button) findViewById(R.id.btn_submit);
		SetTitle("修改手机号");
	}

	private void initEvent() {
		showReturn(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		btn_get_code.setOnClickListener(MyClickListener);
		btn_submit.setOnClickListener(MyClickListener);
		old_phone = MyConstant.userInfo.getPhone();
		txt_phone.setText("当前手机号：" + old_phone);
	}
	
	OnClickListener MyClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent();
			switch (v.getId()) {
			case R.id.btn_get_code:
				btn_get_code.setClickable(false);
				UserHttp.getPhoneCode(Update_Phone.this, http_getPhoneCode);
				current_time = System.currentTimeMillis();
				timeHandler.sendEmptyMessage(0);
				break;
			case R.id.btn_submit:
				if (checkUserInfo()) {
					HashMap map = new HashMap<String, Object>();
					String userMD5 = SPUtil.getString(context, "userMD5", "");
					map.put("oldPhone", old_phone);
					map.put("newPhone", new_phone);
					map.put("oldPwd", MyUtil.stringToMD5(old_phone + old_pwd + userMD5));
					map.put("newPwd", MyUtil.stringToMD5(new_phone + old_pwd + userMD5));
					map.put("checkCode", checkCode);
					UserHttp.updatePhone(Update_Phone.this, map, http_updatePhone);
				}
				break;
			}
		}
	};

	private boolean checkUserInfo() {
		old_pwd = edit_pwd.getText().toString().trim();
		new_phone = edit_new_phone.getText().toString().trim();
		checkCode = edit_check_code.getText().toString().trim();
		if (TextUtils.isEmpty(old_pwd)) {
			ToastUtil.showToast(context, "请输入密码");
			return false;
		}
		if (TextUtils.isEmpty(new_phone)) {
			ToastUtil.showToast(context, "请输入新手机号");
			return false;
		}
		if (TextUtils.isEmpty(checkCode)) {
			ToastUtil.showToast(context, "请输入短信验证码");
			return false;
		}
		return true;
	}

	@Override
	public void onHttpSuccess(int requestCode, Object obj) {
		switch (requestCode) {
		case http_getPhoneCode:
			
			break;
		case http_updatePhone:
			loginOut();
			ToastUtil.showToast(context, "修改成功, 请用新手机号登录", true);
			break;
		}
	}

	@Override
	public void onHttpFail(int requestCode, String errorMsg) {
		switch (requestCode) {
		case http_getPhoneCode:
			ToastUtil.showToast(context, errorMsg);
			break;
		case http_updatePhone:
			ToastUtil.showToast(context, errorMsg);
			break;
		}
	}

}
