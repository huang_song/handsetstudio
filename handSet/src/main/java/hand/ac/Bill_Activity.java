package hand.ac;

import hand.adapter.Bill_Adapter_Car;
import hand.app.UpdateManager;
import hand.http.HandSetHttp;
import hand.http.HttpListener;
import hand.model.BillInfo;
import tt.toolkit.TTUtil;
import tt.toolkit.JsonUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.speedata.libid2.IDInfor;
import com.speedata.libid2.IDManager;
import com.speedata.libid2.IDReadCallBack;
import com.speedata.libid2.IID2Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class Bill_Activity extends BaseActivity implements HttpListener {

	
	private TextView txt_id_info;
	private Button btn_id2;
	private TextView txt_error;
	
	private IID2Service iid2Service;
	private ListView lv_bill;
	private Bill_Adapter_Car billAdapter;
	private List<BillInfo> billList;
//	private List<WareHouse> houseList;
	
	private final int http_getBill = 1;
//	private final int http_getWarehouse = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_tab_car);
		context = this;
		initView();
		initEvent();
	}

	private void initView() {
		txt_id_info = (TextView) findViewById(R.id.txt_id_info);
		btn_id2 = (Button) findViewById(R.id.btn_id2);
		txt_error = (TextView) findViewById(R.id.txt_error);
		lv_bill = (ListView) findViewById(R.id.lv_bill);
		billList = new ArrayList<BillInfo>();
//		houseList = new ArrayList<WareHouse>();
		billAdapter = new Bill_Adapter_Car(context, billList);
		lv_bill.setAdapter(billAdapter);
		SetTitle("装车单");
		showOther("设置");
	}
	
	private void initID() {
		iid2Service = IDManager.getInstance();
		boolean result = false;
		try {
			result = iid2Service.initDev(this, new IDReadCallBack() {
				@Override
				public void callBack(IDInfor idInfo) {
					Message msg = new Message();
					msg.obj = idInfo;
					handler.sendMessage(msg);
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (result) {
			iid2Service.getIDInfor(false);
		} else {
			txt_id_info.setVisibility(View.GONE);
			txt_error.setVisibility(View.VISIBLE);
			txt_error.setText("二代证模块初始化失败！");
			lv_bill.setVisibility(View.GONE);
		}
	}
	
	private void initEvent() {
		btn_id2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				initID();
			}
		});
		
		setCallBack_Other(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(context, Set_Activity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
		});
	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			try {
				iid2Service.releaseDev();
			} catch (IOException e) {
				e.printStackTrace();
			}
			IDInfor idInfo = (IDInfor) msg.obj;
			if (idInfo.isSuccess()) {
				String[] ids = idInfo.getNum().split("\u0000");
				String idCard = "";
				for (String str : ids) {
					idCard += str;
				}
				txt_id_info.setVisibility(View.VISIBLE);
				txt_id_info.setText("姓名：" + idInfo.getName() + "\n身份证号：" + idInfo.getNum() );
//				idCard = "371481199304280319"; //小朱
//				idCard = "370785199211026523"; //晓悦
//				idCard = "34222119901012153X"; //黄宋
//				idCard = "130227198401295214"; //周伟
//				idCard = "132926198010022431"; //洪元
				HandSetHttp.getBill(Bill_Activity.this, idCard, http_getBill);
			} else {
				txt_id_info.setVisibility(View.GONE);
				txt_error.setVisibility(View.VISIBLE);
				txt_error.setText(idInfo.getErrorMsg());
				lv_bill.setVisibility(View.GONE);
			}
		}
	};
	
	@Override
	public void onHttpSuccess(int requestCode, Object obj) {
		String result = (String)obj;
		switch (requestCode) {
		case http_getBill:
			billList = JsonUtil.json2BeanList(result, BillInfo.class);
			if(billList.size() == 0) {
				txt_error.setVisibility(View.VISIBLE);
				txt_error.setText("当前司机无装车单");
				lv_bill.setVisibility(View.GONE);
			} else {
				txt_error.setVisibility(View.GONE);
				lv_bill.setVisibility(View.VISIBLE);
//				HandSetHttp.getWarehouse(this, http_getWarehouse);
				billAdapter.setDatas(billList);
			}
			break;
//		case http_getWarehouse:
//			houseList = JsonUtil.json2BeanList(result, WareHouse.class);;
//			billAdapter.setDatas(billList, houseList);
//			break;
		}
	}

	@Override
	public void onHttpFail(int requestCode, String errorMsg) {
		
	}

	@Override
	protected void onDestroy() {
		try {
			if (iid2Service != null) {
				iid2Service.releaseDev();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			startActivity(intent);
			return false;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (TTUtil.isNetConnected(context)) {
			UpdateManager ud = new UpdateManager(context);
			ud.checkUpdateInfo(false);
		}
	}
	
}
