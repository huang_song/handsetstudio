package hand.ac;

import java.util.Set;
import tt.toolkit.SPUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class Function_Activity extends BaseActivity {

	private TextView txt_login_door;
	private TextView txt_login_car;
	private TextView txt_login_train;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.function_activity);
		tt.util.ActivityCollector.addActivity(this);
		context = Function_Activity.this;
		txt_login_door = (TextView) findViewById(R.id.txt_login_door);
		txt_login_car = (TextView) findViewById(R.id.txt_login_car);
		txt_login_train = (TextView) findViewById(R.id.txt_login_train);
		initEvent();
	}
	
	private void initEvent() {
		Set<String> set = SPUtil.getStringSet(context, "funcNodesp");
		for (String str : set) {
			switch (str) {
			case "doorkeeper":
				txt_login_door.setVisibility(View.VISIBLE);
				txt_login_door.setOnClickListener(myClickListener);
				break;
			case "vehicle":
				txt_login_car.setVisibility(View.VISIBLE);
				txt_login_car.setOnClickListener(myClickListener);
				break;
			case "train":
				txt_login_train.setVisibility(View.VISIBLE);
				txt_login_train.setOnClickListener(myClickListener);
				break;
			}
		}
	}

	OnClickListener myClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent();
			switch (v.getId()) {
			case R.id.txt_login_door:
				intent.setClass(context, Index_Activity_Door.class);
				startActivity(intent);
				break;
			case R.id.txt_login_car:
				intent.setClass(context, Index_Activity_Car.class);
				startActivity(intent);
				break;
			case R.id.txt_login_train:
				intent.setClass(context, Index_Activity_Train.class);
				startActivity(intent);
				break;
			}
		}
	};

}
