package hand.ac;

import java.util.HashMap;
import hand.app.MyConstant;
import hand.http.HttpListener;
import hand.http.UserHttp;
import hand.util.MyUtil;
import tt.toolkit.SPUtil;
import tt.toolkit.ToastUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Update_Pwd extends BaseActivity {

	private EditText et_old_pwd;
	private EditText et_new_pwd;
	private EditText et_confirm_pwd;
	private Button btn_submit;
	private final int http_updatePwd = 1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.update_password);
		context = Update_Pwd.this;
		initView();
	}

	private void initView() {
		et_old_pwd = (EditText) findViewById(R.id.et_old_pwd);
		et_new_pwd = (EditText) findViewById(R.id.et_new_pwd);
		et_confirm_pwd = (EditText) findViewById(R.id.et_confirm_pwd);
		btn_submit = (Button) findViewById(R.id.btn_submit);
		SetTitle("修改密码");
		showReturn(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		btn_submit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				updatePassword();
			}
		});
	}

	private void updatePassword() {
		String oldPwd = et_old_pwd.getText().toString().trim();
		String newPwd = et_new_pwd.getText().toString().trim();
		String confirmPwd = et_confirm_pwd.getText().toString().trim();
		
		if (TextUtils.isEmpty(oldPwd)) {
			ToastUtil.showToast(context, "请输入原密码！");
			return;
		}
		if (TextUtils.isEmpty(newPwd)) {
			ToastUtil.showToast(context, "请输入新密码！");
			return;
		}
		if (TextUtils.isEmpty(confirmPwd)) {
			ToastUtil.showToast(context, "请输入确认密码！");
			return;
		}
		if (!newPwd.equals(confirmPwd)) {
			ToastUtil.showToast(context, "两次密码输入不一致！");
			return;
		}
		
		String phone = MyConstant.userInfo.getPhone();
		String userMD5 = SPUtil.getString(context, "userMD5", null);
		String oldPassword = MyUtil.stringToMD5(phone + oldPwd + userMD5);
		String newPassword = MyUtil.stringToMD5(phone + newPwd + userMD5);
		HashMap map = new HashMap<String, String>();
		map.put("oldPassword", oldPassword);
		map.put("newPassword", newPassword);

		UserHttp.updatePwd(new HttpListener() {
			@Override
			public void onHttpSuccess(int requestCode, Object obj) {
				ToastUtil.showToast(context, "更新成功");
				finish();
			}
			
			@Override
			public void onHttpFail(int requestCode, String errorMsg) {
				ToastUtil.showToast(context, errorMsg);
			}
		}, map, http_updatePwd);
		
	}

}
