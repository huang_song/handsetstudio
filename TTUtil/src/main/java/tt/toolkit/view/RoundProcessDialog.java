package tt.toolkit.view;

import tt.util.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnDismissListener;
import android.view.View;
import android.widget.TextView;

public class RoundProcessDialog {

	private Dialog mDialog;
	
	/** dialog 消失 */
	@SuppressLint("NewApi")
	public void dismissRoundProcessDialog() {
		if (mDialog == null) {
			return;
		}
		if (mDialog.getContext() instanceof Activity) {
			// activity是否已经关闭
			if (((Activity) mDialog.getContext()).isFinishing()) {
				return;
			}
		}
		if (mDialog != null && mDialog.isShowing()) {
			mDialog.dismiss();
		}
		mDialog = null;
	}

	/** 显示 dialog */
	public Dialog showRoundProcessDialog(Context context, OnDismissListener listener) {
		if (mDialog != null && mDialog.isShowing()) {
			return mDialog;
		}
		if (context == null || ((Activity) context).isFinishing()) {
			return mDialog;
		}
		if (mDialog == null) {
			mDialog = getLoadingDialog(context, "");
		}
		if (listener != null) {
			mDialog.setOnDismissListener(listener);
		}
		mDialog.setCancelable(true);
		mDialog.show();
		return mDialog;
	}

	/** 显示 dialog */
	public Dialog showRoundProcessDialog(Context context, OnDismissListener listener, boolean isCancancel) {
		if (context == null || ((Activity) context).isFinishing()) {
			return mDialog;
		}
		if (mDialog == null) {
			mDialog = getLoadingDialog(context, "");
		}
		if (listener != null) {
			mDialog.setOnDismissListener(listener);
		}
		mDialog.setCancelable(isCancancel);
		mDialog.show();
		return mDialog;
	}
	
	
	/** 自定义等待 dialog */
	public static Dialog getLoadingDialog(Context context, String msg) {
		Dialog dialog = new Dialog(context, R.style.loading_dialog);
		dialog.setContentView(R.layout.loading_dialog);
		dialog.setCancelable(true);
		TextView tvText = (TextView) dialog.findViewById(R.id.dialog_tv);
		if (msg != null) {
			tvText.setText(msg);
		} else {
			tvText.setVisibility(View.GONE);
		}
		return dialog;
	}

}