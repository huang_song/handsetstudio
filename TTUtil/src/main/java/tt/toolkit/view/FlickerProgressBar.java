package tt.toolkit.view;

import tt.util.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;

public class FlickerProgressBar extends View implements Runnable {
	
	private RectF bgRectf;
	private Paint bgPaint;
	
	private Canvas pCanvas;
	private Paint pPaint;
	private Bitmap pBitmap; 	 // 进度条bitmap，包含滑块
	
	private Bitmap flikerBitmap; // 左右来回移动的滑块
	private float flickerLeft;	 // 滑块移动最左边位置
	
	private Rect textRect;
	private Paint textPaint;
	private float progress;		// 当前进度
	public boolean isFinish;

	private int progressColor;	// 进度文本、边框、进度条颜色
	private int textSize;
	private int radius;
	private int borderWidth;
	
	private Thread thread;
	private float maxProgress = 100f;

	public FlickerProgressBar(Context context) {
		this(context, null, 0);
	}

	public FlickerProgressBar(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public FlickerProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initAttrs(context, attrs);
	}

	private void initAttrs(Context context, AttributeSet attrs) {
		TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.FlickerProgressBar);
		progressColor = ta.getColor(R.styleable.FlickerProgressBar_progressColor, Color.parseColor("#40c4ff"));
		textSize = (int) ta.getDimension(R.styleable.FlickerProgressBar_textSize, 12);
		radius = (int) ta.getDimension(R.styleable.FlickerProgressBar_radius, 0);
		borderWidth = (int) ta.getDimension(R.styleable.FlickerProgressBar_borderWidth, 1);
		ta.recycle();
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int widthSpecSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightSpecSize = MeasureSpec.getSize(heightMeasureSpec);
		int heightSpecMode = MeasureSpec.getMode(heightMeasureSpec);
		int height = 0;
//		System.out.println("widthMeasureSpec:" + widthMeasureSpec + "  widthSpecSize:" + widthSpecSize);
//		System.out.println("heightMeasureSpec:" + heightMeasureSpec + "  heightSpecSize:" + heightSpecSize);
//		System.out.println("heightSpecMode:" + heightSpecMode);
		switch (heightSpecMode) {
		case MeasureSpec.AT_MOST:
			height = dp2px(20);
			break;
		case MeasureSpec.EXACTLY:
		case MeasureSpec.UNSPECIFIED:
			height = heightSpecSize;
			break;
		}
		setMeasuredDimension(widthSpecSize, height);

		if (pBitmap == null) {
			init();
		}
	}
	
	private void init() {
//		System.out.println("getMeasuredWidth-- " + getMeasuredWidth());
//		System.out.println("getMeasuredHeight-- " + getMeasuredHeight());
		
		bgRectf = new RectF(borderWidth, borderWidth, getMeasuredWidth() - borderWidth, getMeasuredHeight() - borderWidth);
		bgPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG);
		bgPaint.setStyle(Paint.Style.STROKE);
		bgPaint.setStrokeWidth(borderWidth);
		bgPaint.setColor(progressColor);

		pBitmap = Bitmap.createBitmap(getMeasuredWidth() - borderWidth, getMeasuredHeight() - borderWidth, Bitmap.Config.ARGB_8888);
		pCanvas = new Canvas(pBitmap);
		pPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		pPaint.setStyle(Paint.Style.FILL);
		pPaint.setColor(progressColor);

		textRect = new Rect();
		textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		textPaint.setTextSize(textSize);

		flikerBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.flicker);
		flickerLeft = -flikerBitmap.getWidth();
		
		thread = new Thread(this);
		thread.start();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawRoundRect(bgRectf, radius, radius, bgPaint); // 背景边框
		drawProgress(canvas);		// 进度
		drawProgressText(canvas);	// 进度text
	}

	/** 进度 */
	private void drawProgress(Canvas canvas) {
		float progressWidth = (progress / maxProgress) * getMeasuredWidth();
		pCanvas.save(Canvas.CLIP_SAVE_FLAG);
		pCanvas.clipRect(0, 0, progressWidth, getMeasuredHeight());
		pCanvas.drawColor(progressColor);
		pCanvas.restore();
		
		if (!isFinish) {
			pPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));
			pCanvas.drawBitmap(flikerBitmap, flickerLeft, 0, pPaint);
			pPaint.setXfermode(null);
		}

		// 把进度添加到主canvas上
		BitmapShader bitmapShader = new BitmapShader(pBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
		pPaint.setShader(bitmapShader);
		canvas.drawRoundRect(bgRectf, radius, radius, pPaint);
//		(该情况下，上面3行显示效果，同下面3行)
//		pPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));
//		canvas.drawBitmap(pBitmap, borderWidth, borderWidth, pPaint);
//		pPaint.setXfermode(null);
		
//		Shader本身是一个抽象类,它提供了如下实现类:
//		> BitmapShader: 使用位图平铺的渲染效果
//		> LinearGradient: 使用线性渐变来填充图形
//		> RadialGradient: 使用圆形渐变来填充图形
//		> SweepGradient: 使用角度渐变来填充图形
//		> ComposeShader: 使用组合渲染效果来填充图形
	}

	/** 进度提示文本 */
	private void drawProgressText(Canvas canvas) {
		String progressText;
		if (isFinish) {
			progressText = "下载完成";
		} else {
			progressText = "下载中" + progress + "%";
		}
		
		textPaint.getTextBounds(progressText, 0, progressText.length(), textRect);
		int tWidth = textRect.width();
		int tHeight = textRect.height();
		float xCoordinate = (getMeasuredWidth() - tWidth) / 2;
		float yCoordinate = (getMeasuredHeight() + tHeight) / 2;
		
		textPaint.setColor(progressColor);
		canvas.drawText(progressText, xCoordinate, yCoordinate, textPaint);
		
		 // 变色处理
		textPaint.setColor(Color.WHITE);
		float progressWidth = (progress / maxProgress) * getMeasuredWidth();
		if (progressWidth > xCoordinate) {
			float right = Math.min(progressWidth, xCoordinate + tWidth * 1.1f);
			canvas.save(Canvas.CLIP_SAVE_FLAG);
			canvas.clipRect(xCoordinate, 0, right, getMeasuredHeight());
			canvas.drawText(progressText, xCoordinate, yCoordinate, textPaint);
			canvas.restore();
		}
	}

	public void setProgress(float progress) {
		this.progress = progress;
		invalidate();
	}
	
	public void setFinish() {
		isFinish = true;
		invalidate();
	}

	@Override
	public void run() {
		try {
			while (!isFinish) {
				flickerLeft += dp2px(5);
				float progressWidth = (progress / maxProgress) * getMeasuredWidth();
				if (flickerLeft >= progressWidth) {
					flickerLeft = -flikerBitmap.getWidth();
				}
				postInvalidate();
				Thread.sleep(20);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private int dp2px(int dp) {
		float density = getContext().getResources().getDisplayMetrics().density;
		return (int) (dp * density);
	}

}
