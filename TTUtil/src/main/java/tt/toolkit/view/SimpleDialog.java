package tt.toolkit.view;

import tt.util.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;

public class SimpleDialog {

	private SimpleDialog() { }

	public static SimpleDialog getInstance() {
		return new SimpleDialog();
	}

	/** 确定回调方法 */
	public interface OnPositiveClickListener {
		public void onPositiveClick();
	}

	/** 取消回调方法 */
	public interface OnNegativeClickListener {
		public void onNegativeClick();
	}
	
	private OnPositiveClickListener positiveListener;
	private OnNegativeClickListener negativeListener;

	public void setPositiveListener(OnPositiveClickListener listener) {
		this.positiveListener = listener;
	}

	public void setNegativeListener(OnNegativeClickListener listener) {
		this.negativeListener = listener;
	}

	/** 显示一个简单的dialog */
	public void showSimpleDialog(Context ctx, String message) {
		showSimpleDialog(ctx, message, null);
	}

	/**
	 * 显示一个简单的dialog
	 * 
	 * @param couldCancel
	 *            能否在外部触摸模式下取消，默认不能
	 */
	public void showSimpleDialog(Context ctx, String message, Boolean couldCancel) {
		final Dialog dialog = new AlertDialog.Builder(ctx).create();
		dialog.setCancelable(couldCancel == null ? false : couldCancel);
		dialog.show();
		dialog.setContentView(R.layout.dialog_simple);
		TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
		TextView txt_sure = (TextView) dialog.findViewById(R.id.txt_sure);
		txt_title.setText(message);
		txt_sure.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if (positiveListener != null) {
					positiveListener.onPositiveClick();
				}
			}
		});
	}

	
	/** 显示一个可选的dialog */
	public void showChooseDialog(Context ctx, String message) {
		showChooseDialog(ctx, message, null, null, null);
	}

	/** 显示一个可选的dialog */
	public void showChooseDialog(Context ctx, String message, String txtCancel, String txtSure) {
		showChooseDialog(ctx, message, txtCancel, txtSure, null);
	}

	/**
	 * 显示一个可选的dialog
	 * 
	 * @param couldCancel
	 *            能否在外部触摸模式下取消，默认不能
	 */
	public Dialog showChooseDialog(Context ctx, String message, String txtCancel, String txtSure, Boolean couldCancel) {
		final Dialog dialog = new AlertDialog.Builder(ctx).create();
		dialog.setCancelable(couldCancel == null ? false : couldCancel); // true屏幕+返回键都可以取消dialog；false屏幕+返回键都不可以取消dialog
		// dialog.setCanceledOnTouchOutside(true); //
		// true屏幕+返回键都可以取消dialog；false屏幕不可以取消dialog，返回键可以取消dialog
		dialog.show();
		dialog.setContentView(R.layout.dialog_choose);
		int width = dip2px(ctx, 320);
		int height = dip2px(ctx, 140);
		Window window = dialog.getWindow();
		window.setLayout(width, height);
		window.setGravity(Gravity.CENTER);

		TextView txt_title = (TextView) dialog.findViewById(R.id.txt_title);
		TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);
		TextView txt_sure = (TextView) dialog.findViewById(R.id.txt_sure);
		txt_title.setText(message);
		if (!TextUtils.isEmpty(txtCancel)) {
			txt_cancel.setText(txtCancel);
		}
		if (!TextUtils.isEmpty(txtSure)) {
			txt_sure.setText(txtSure);
		}

		txt_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if (negativeListener != null) {
					negativeListener.onNegativeClick();
				}
			}
		});
		txt_sure.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if (positiveListener != null) {
					positiveListener.onPositiveClick();
				}
			}
		});
		return dialog;
	}
	
	/** 将dip值动态转换成px值 */
	public static int dip2px(Context context, float dpValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dpValue * scale + 0.5f);
	}
	
}
