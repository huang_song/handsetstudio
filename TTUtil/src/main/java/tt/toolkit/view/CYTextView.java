package tt.toolkit.view;

import tt.util.R;
import java.util.Vector;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.util.AttributeSet;
import android.widget.TextView;

/** 
 * TextView或EditView换行文字排版不齐
 * 原因1：半角字符与全角字符混乱所致：这种情况一般就是汉字与数字、英文字母混用
 * 原因2：TextView在显示中文的时候标点符号不能显示在一行的行首和行尾，如果一个标点符号刚好在一行的行尾，该标点符号就会连同前一个字符跳到下一行显示
 * 原因3：一个英文单词不能被显示在两行中（TextView在显示英文时，标点符号是可以放在行尾的，但英文单词也不能分开）
 */  

public class CYTextView extends TextView {
	public static int m_iTextHeight; // 文本的高度
	public static int m_iTextWidth;// 文本的宽度

	private Paint mPaint = null;
	private String string = "";
	private float LineSpace = 0;// 行间距

	public CYTextView(Context context, AttributeSet set) {
		super(context, set);
		TypedArray typedArray = context.obtainStyledAttributes(set, R.styleable.CYTextView);
		float textsize = typedArray.getDimension(R.styleable.CYTextView_mytextSize, 20);
		int textcolor = typedArray.getColor(R.styleable.CYTextView_mytextColor, -1442840576);
		float linespace = typedArray.getDimension(R.styleable.CYTextView_lineSpacingExtra, 6);
		typedArray.recycle();
		LineSpace = linespace;
		// 构建paint对象
		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setColor(textcolor);
		mPaint.setTextSize(textsize);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (m_iTextWidth <= 0) {
			return;
		}
		char ch;
		int w = 0;
		int istart = 0;
		int m_iFontHeight;
		int m_iRealLine = 0;
		int x = 2;
		int y = 30;

		Vector m_String = new Vector();

		FontMetrics fm = mPaint.getFontMetrics();
		m_iFontHeight = (int) Math.ceil(fm.descent - fm.top) + (int) LineSpace;// 计算字体高度（字体高度＋行间距）
		y = (int) Math.ceil(fm.descent - fm.top);
		for (int i = 0; i < string.length(); i++) {
			ch = string.charAt(i);
			float[] widths = new float[1];
			String srt = String.valueOf(ch);
			mPaint.getTextWidths(srt, widths);

			if (ch == '\n') {
				m_iRealLine++;
				m_String.addElement(string.substring(istart, i));
				istart = i + 1;
				w = 0;
			} else {
				w += (int) (Math.ceil(widths[0]));
				if (w > m_iTextWidth) {
					m_iRealLine++;
					m_String.addElement(string.substring(istart, i));
					istart = i;
					i--;
					w = 0;
				} else {
					if (i == (string.length() - 1)) {
						m_iRealLine++;
						m_String.addElement(string.substring(istart, string.length()));
					}
				}
			}
		}
		m_iTextHeight = m_iRealLine * m_iFontHeight + 2;
		for (int i = 0, j = 0; i < m_iRealLine; i++, j++) {
			canvas.drawText((String) (m_String.elementAt(i)), x, y + m_iFontHeight * j, mPaint);
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int measuredWidth = measureWidth(widthMeasureSpec);
		m_iTextWidth = measuredWidth;
		int measuredHeight = measureHeight(heightMeasureSpec);
		this.setMeasuredDimension(measuredWidth, measuredHeight);
	}

	private int measureHeight(int measureSpec) {
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);
		initHeight();
		int result = m_iTextHeight;
		if (specMode == MeasureSpec.AT_MOST) {
			result = specSize;
		} else if (specMode == MeasureSpec.EXACTLY) {
			result = specSize;
		}
		return result;
	}

	private void initHeight() {
		// 设置 CY TextView的初始高度为0
		m_iTextHeight = 0;
		// 大概计算 CY TextView所需高度
		FontMetrics fm = mPaint.getFontMetrics();
		int m_iFontHeight = (int) Math.ceil(fm.descent - fm.top) + (int) LineSpace;
		int line = 0;
		int istart = 0;

		int w = 0;
		for (int i = 0; i < string.length(); i++) {
			char ch = string.charAt(i);
			float[] widths = new float[1];
			String srt = String.valueOf(ch);
			mPaint.getTextWidths(srt, widths);

			if (ch == '\n') {
				line++;
				istart = i + 1;
				w = 0;
			} else {
				w += (int) (Math.ceil(widths[0]));
				if (w > m_iTextWidth) {
					line++;
					istart = i;
					i--;
					w = 0;
				} else {
					if (i == (string.length() - 1)) {
						line++;
					}
				}
			}
		}
		m_iTextHeight = (line) * m_iFontHeight + 2;
	}

	private int measureWidth(int measureSpec) {
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);
		int result = 500;
		if (specMode == MeasureSpec.AT_MOST) {
			result = specSize;
		} else if (specMode == MeasureSpec.EXACTLY) {
			result = specSize;
		}
		return result;
	}

	public void SetText(String text) {
		string = text;
		requestLayout();
		invalidate();
	}
}
