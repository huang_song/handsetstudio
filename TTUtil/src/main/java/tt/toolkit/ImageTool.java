package tt.toolkit;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.util.Base64;
import android.util.DisplayMetrics;

public final class ImageTool {
	
	public static void saveBitmap2File(Context context, Bitmap bitmap, String filepath, String filename) {
		if (bitmap == null) {
			return;
		}
		File dirFile = new File(filepath);
		if (!dirFile.exists()) {
			dirFile.mkdir();
		}
		File file = new File(filepath, filename);
		if (file.exists()) {
			file.delete();
		}
		try {
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(file);
			if (bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream)) {
				fileOutputStream.flush();
				fileOutputStream.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 描述：缩放图片.压缩
	 * 
	 * @param file
	 *            File对象
	 * @param width
	 *            新图片的宽
	 * @param height
	 *            新图片的高
	 * @return Bitmap 新图片
	 */
	public static Bitmap getBitmapfromFile(String filepath, int width, int height) {
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inJustDecodeBounds = true; // 设置为true，decodeFile先不创建内存，获取Bitmap的外围数据，宽和高等
		opts.inPreferredConfig = Bitmap.Config.RGB_565; // 默认为ARGB_8888
		BitmapFactory.decodeFile(filepath, opts);

		float scaleWidth = (float) opts.outWidth / width;
		float scaleHeight = (float) opts.outHeight / height;
		float scale = scaleWidth > scaleHeight ? scaleWidth : scaleHeight;

		// 4）inPurgeable设为true的话，则由此产生的位图将分配其像素，在系统内存不足时可以被回收
		// 5）inInputShareable与inPurgeable一起使用，如果inPurgeable为false那该设置将被忽略
		// 如果为true，那么它可以决定位图是否能够共享一个指向数据源的引用，或者是进行一份拷贝
		// 位图可以共享一个参考输入数据(inputStream、阵列等)
		// purgeable可清除的，可净化的
		opts.inPurgeable = true;
		opts.inInputShareable = true;

		// inSampleSize 只支持2的n次方，其它数值向下取最接近2的整数次方
		// inSampleSize=2 表示图片宽高都为原来的二分之一，即图片为原来的四分之一
		opts.inSampleSize = (int) scale; // 缩小
		opts.inDither = false; // 不进行图片抖动处理
		opts.inJustDecodeBounds = false;

		return BitmapFactory.decodeFile(filepath, opts);
	}

	/**
	 * 缩放bitmap到指定的尺寸
	 * 
	 * @param bitmap
	 *            需要缩放的图片源
	 * @return Bitmap 缩放后的图片
	 */
	public static Bitmap scaleBitmap(Bitmap bitmap, Activity activity) {
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		float scale = (float) getScreenWidth(activity) / width; // 计算缩放比例
		if (scale > 1.0) {
			scale = 0.75f;
		}

		Matrix matrix = new Matrix();
		matrix.postScale(scale, scale); // 宽高的缩放比例，如缩小0.8，放大1.2
		return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
	}

	/**
	 * 质量压缩
	 * 
	 * @param bitmap
	 * @param compress
	 *            传递的质量
	 * @return Bitmap
	 */
	public static Bitmap myCompress(Bitmap bitmap, int compress) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		// 质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
		int quality = 100;
		bitmap.compress(Bitmap.CompressFormat.JPEG, quality, baos);
		while (baos.toByteArray().length / 1024 > compress) {
			baos.reset(); // 重置baos即清空baos
			quality -= 10;
			bitmap.compress(Bitmap.CompressFormat.JPEG, quality, baos);
		}

		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());

		return BitmapFactory.decodeStream(bais, null, null);
	}

	public static byte[] resource2Bytes(Context context, int resId) {
		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resId);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
		return baos.toByteArray();
	}

	public static Bitmap getBitmapfromResource(Context context, int resId) {
		return BitmapFactory.decodeResource(context.getResources(), resId);
	}
	
	public static Bitmap getBitmapfromURL(URL url) {
		try {
			return BitmapFactory.decodeStream(url.openStream());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 回收bitmap
	 * 
	 * @param bitmap
	 * @param useGc
	 *            是否调用System.gc();
	 */
	public static void recycleBitmap(Bitmap bitmap, boolean useGc) {
		if (bitmap == null)
			return;
		if (!bitmap.isRecycled()) {
			bitmap.recycle();
			bitmap = null;
		}
		if (useGc)
			System.gc();
	}

	/**
	 * bitmap转为base64
	 * 
	 * @param bitmap
	 * @return
	 */
	public static String bitmapToBase64(Bitmap bitmap) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
	}
	
	
	public static int readPictureDegree(String path) {
		int degree = 0;
		try {
			ExifInterface exifInterface = new ExifInterface(path);
			int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);
			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_90:
				degree = 90;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				degree = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_270:
				degree = 270;
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return degree;
	}
	
	// 得到屏幕宽度
	public static int getScreenWidth(Activity activity) {
		DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int width = metrics.widthPixels;
		return width;
	}

}
