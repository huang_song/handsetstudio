package tt.toolkit;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.http.conn.ssl.SSLSocketFactory;

public class TrustAllSSLSocketFactory extends SSLSocketFactory {

	private SSLContext sslCtx;

	public TrustAllSSLSocketFactory(KeyStore truststore) throws Throwable {
		super(truststore);
		try {
			sslCtx = SSLContext.getInstance("TLS");
			sslCtx.init(null, new TrustManager[] { new TrustAllManager() }, null);
			setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public class TrustAllManager implements X509TrustManager {
		@Override
		public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException { }
		@Override
		public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException { }
		@Override
		public X509Certificate[] getAcceptedIssuers() {
			return null;
		}
	}

	public static SSLSocketFactory getSocketFactory() {
		try {
			KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
			keyStore.load(null, null);
			SSLSocketFactory factory = new TrustAllSSLSocketFactory(keyStore);
			return factory;
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public Socket createSocket() throws IOException {
		return sslCtx.getSocketFactory().createSocket();
	}
	
	@Override
	public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException,
			UnknownHostException {
		return sslCtx.getSocketFactory().createSocket(socket, host, port, autoClose);
	}

}
