package tt.toolkit;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

/** 避免Toast连续多次弹出 */

public class ToastUtil {
	
	private static Toast toast;

	public static void showToast(Context context, String msg) {
		if (TextUtils.isEmpty(msg)) {
			return;
		}
		if (toast == null) {
			toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
		} else {
			toast.setDuration(Toast.LENGTH_SHORT);
		}
		toast.setText(msg);
		toast.show();
	}

	public static void showToast(Context context, String msg, boolean isLongShow) {
		if (TextUtils.isEmpty(msg)) {
			return;
		}
		if (isLongShow) {
			if (toast == null) {
				toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
			} else {
				toast.setDuration(Toast.LENGTH_LONG);
			}
			toast.setText(msg);
			toast.show();
		}
	}

	public static void cancel() {
		if (toast != null) {
			toast.cancel();
		}
	}
	
}
