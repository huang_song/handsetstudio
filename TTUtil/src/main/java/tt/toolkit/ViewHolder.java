package tt.toolkit;

import android.util.SparseArray;
import android.view.View;

/** ViewHold简写工具类 */

public class ViewHolder {

	/**
	 * @param view
	 *            convertView
	 * @param id
	 *            控件
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T extends View> T get(View view, int id) {
		SparseArray<View> viewHolder = (SparseArray<View>) view.getTag();
		if (viewHolder == null) {
			viewHolder = new SparseArray<View>();
			view.setTag(viewHolder);
		}
		View childView = viewHolder.get(id);
		if (childView == null) {
			childView = view.findViewById(id);
			viewHolder.put(id, childView);
		}
		return (T) childView;
	}
	
}
