package tt.toolkit;

import android.util.Log;

public class LogUtil {
	
	private static boolean isLogOpen = true;

	public static void i(String tag, String msg) {
		if (isLogOpen) {
			Log.i(tag, msg == null ? "null" : msg);
		}
	}

	public static void i(String tag, Object msg) {
		if (isLogOpen) {
			Log.i(tag, msg == null ? "null" : msg.toString());
		}
	}

	public static void i(String tag, int msg) {
		if (isLogOpen) {
			Log.i(tag, String.valueOf(msg));
		}
	}
	

	public static void e(String tag, String msg) {
		if (isLogOpen) {
			Log.e(tag, msg == null ? "null" : msg);
		}
	}

	public static void e(String tag, Object msg) {
		if (isLogOpen) {
			Log.e(tag, msg == null ? "null" : msg.toString());
		}
	}

	public static void e(String tag, int msg) {
		if (isLogOpen) {
			Log.e(tag, String.valueOf(msg));
		}
	}
}
